# Orkward
## The Space Ork Dating Game

Orkward is a short interactive fiction game about "dating" five space orks. It has three major endings which unite the game into a cohesive storyline.

### Getting Started

`npm run start` will run the dev server with hot reloading on port 8080
`npm run build` will build a distributable static HTML 
`npm run build:ink` will force the ink source files to be compiled into JSON
`npm run deploy <version>` will deploy /dist to itch.io

I'm favouring npm these days, but the above should all work with yarn.

### Project & Motivation

Orkward has been in development since October 2020. It's a hobby project I've undertaken while on sabbatical. I needed a project to work on which was creative and fun without being too stressful. The mix of JavaScript and creative writing here is just perfect for me right now and I'm having a great time.

### Tech

There are two parts to the game.

The text is (mostly) displayed using Ink, a scripting langauge for writing interactive narratives by Ink. Ink compiles to Javascript and runs in a webpage. I've taken the basic output and modified it to suit my own needs - this includes a React wrapper around the ink runtime (there's a lot to do here but what's done serves my needs, which is all I want for now).

The game is driven by a custom frontend written in React. It sandwiches the ink runtime between a gallery menu and some intro/outro pages, as well as adding some UI for trophies and credits.

At dev-time, I have a hot-loading webpack setup which refreshes the page when something loads. Ink source files are compiled into JSON on save (driven through VSC).

Unfortunately it's a bit hard to nicely hot-load the ink runtime. The best I've been able to do so far is to just reload the story when the underlying JSON changes. It should be possible to just re-generate the most recent "page", but I abandoned some experiments around that because it just wasn't work the time :(

### Repo

Some repo highlights:

* ink: source for the story text
* dist: a static build of the game, for distribution. Everything in here is generated
* lib: the ink runtime and the compiled story json
* src: The js/jsx source of the actual frontend

### Tests

Yeah, there aren't any unit tests. I'm not even sure what I'd test. I could add some Jest snapshots but I think it's quite low value to me now. It would be interesting the test the story logic somehow, but working out how to do that is a project in itself.