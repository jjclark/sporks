#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "ERROR - no build number!"
    exit 1
fi

butler push ../dist monstro/orkward:web --userversion $1