cd ../img
ROOT=`pwd`

names=(wendy krugg chompa skullkrusha sharptoof)
for n in "${names[@]}"; do
    magick $ROOT/colored/$n-small.png -resize 350x350 $ROOT/scaled/$n-350x.png
    magick $ROOT/colored/$n-small-outline.png -resize 350x350 $ROOT/scaled/$n-350x-outline.png
    magick $ROOT/colored/$n.png -resize 1000x1000 $ROOT/scaled/$n-1000x.png
done 
