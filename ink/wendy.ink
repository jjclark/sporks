/*
    Wendy likes hunting and eating.
    
    If you earn his trust than you can go hunting with him, whereupon he'll be killed.
    
    You just have to show you're tough - either through convo or by surving getting shot at (you need armour for this).

    When hunting with Wendy, there are a couple of rules:
    - Wendy's advice should clearly be terrible
    - Doing what Wendy says will get you killed
    - Doing the opposite of what Wendy says is the safest and easiest way to succed
    - If you out-Wendy Wendy, by doubling-down on his advice, you'll unlock the Way of Wendy

    Is it a problem that to get the True Ending you have to follow wendy's instruction? I guess not, to get Way of Wendy you always do what wendy says...

    TODO I think I want to make it harder (and more dangerous) to learn wendy's name

    TODO get rid of the idea of negging wendy to get his respect

    TODO gambling scene
*/

VAR persist_wendy_dislikes_name = false
VAR persist_wendy_dislikes_weird = false
VAR persist_wendy_likes_hunting = false
VAR persist_wendy_likes_eating = false
VAR persist_wendy_dislikes_softness = false
VAR persist_wendy_failed_hunts = 0

VAR knife_name = "your knife"

VAR wendy_way_score = 0
VAR wendy_respect_score = 0
VAR wendy_annoyed = 0
VAR wendy_disappointed = false
VAR wendy_respect = false

// TODO need to think more about what happens when you come back after being too soft
// Can you recover it?

== function status_wendy ==
{
    -obj_wendy_dead:
        ~return "Wendy's firepit is cold and dark.'"
    -obj_met_wendy:
        ~return "Wendy stands by the fire, chewing on a Nickleback leg"
    -else:
        ~ return "An ork stands by a fire, chewing on... something"
}

== Wendy

// TODO is there a better way to deal with this loop? That's exactly the sort of thing I want to be learning...
VAR first = true

= profile

Wendy's Profile

Likes:
    \* Shootin'
    \* { -persist_wendy_likes_hunting:
        <> Huntin'
        -else: <> ???
        }
    \* { -persist_wendy_likes_eating:
    <> Eatin'
    -else: <> ???
    }

Dislikes:

    \* { -persist_wendy_dislikes_weird:
    <> Weirdboyz
    -else: <> ???
    }
    \* { -persist_wendy_dislikes_name:
    <> Being teased about his name
    -else: <> ???
    }

+ [<< BACK]
-> main

= start

{ 
-obj_wendy_dead:
    -> cold_fireplace

-wendy_respect:
    Wendy looks up as you approach and nods his head.

    You take a seat next to him. He offers you a steaming chunk of meat.

    + "Wanna play cards?" # GUIDE found_money/start
        -> play_cards

    + [Hang out.]
    
        You talk occasionally, but mostly enjoy the silence together.

        -> go_to_main
-wendy_disappointed:
    Wendy doesn't look up as you approach.

    You try and get his attention, but the stoic ork refuses to acknowledge your presence.

    // TODO wendy gets over it after a while I think?
    -> go_to_main
-obj_met_wendy:
    Wendy nods as you take a seat by the fire.
    // TODO some preamble
    -> wendy_menu
-first:
    An ork sits on a rock beside a firepit at the edge of the camp, dressed in black rags, an eyepatch covering one eye.
    
    The ork chews noisily at a charred hunk of meat on a bone.
    
    It smells amazing.
    ~first = false
- else: 
    The ork ignores you and continues to chew at his food.
}

-> root_menu

= cold_fireplace

Wendy's fireplace is cold and quiet.

Charred Nickleback bones lie amongst the ashes.

+ [Go back]
    -> main

= are_you_weird

The ork squints over his food.

"Is you weird or somefink?"

He said, lowering the meat and resting one hand on the pistol on his belt.

"I hate weird boyz."

// TODO maybe make this more interesting? Allow to survive this path, but allow others
// maybe it's three strikes and you're out
+ ["I ain't weird!"] "I ain't we-" you start.
    -> killed_by_wendy
+ ["Nah, Oi'm tuff, just like you"] "I ain't we-" you begin.
    -> killed_by_wendy
+ ["Why no, good sir! Please don't take offence at my tumescent nature"] "Why no-" you gasp.
    -> killed_by_wendy

= killed_by_wendy
~ persist_wendy_dislikes_weird = true

Quick as a flash, Wendy draws the pistol and shoots you square between the eyes.

You collapse into the dirt.

-> death

= ignore
~wendy_annoyed++
{
  - wendy_annoyed > 3:
    -> ignore_death
  - wendy_annoyed > 2:
    "Look pal," the ork grunts. "I just wanna eat in peace, alright? Shaddap, shove off, or get ready fer a stabbin'."

    At least it's conversation.
  - else:
    {The ork ignores you.|The ork stares into the middle distance.}
}

-> root_menu

= ignore_death

"Dat's it!" Wendy says, throwing his meat to the floor and standing quickly to his feet.

* [Blink] You blink.

- When your eyes flick open, they are staring down the barrel of a pistol.

"I told ya to shaddap," Wendy says. Then pulls the trigger.
    -> death

= sit_down

~wendy_respect_score++
The ork grunts, but does not object.

-> root_menu

= meet_the_meat

~wendy_respect_score++

"Dat's prime Nickleback leg," the ork grunts proudly as you eye the steaming meat. "Noice 'n' fresh!" he beams.

-> root_menu

= root_menu_debug
~wendy_respect_score = 3

-> root_menu

= root_menu

// chatter
* { wendy_annoyed == 0 } "Hello[."]," you say. -> ignore
* { wendy_annoyed == 1 } "Wot you eatin?"[] you ask. -> ignore
* { wendy_annoyed == 2 } "Nice day fer it[."]," you say. -> ignore
* { wendy_annoyed == 3 } "So what's good around 'ere?"[] you ask. # GUIDE death/wendy_annoyed
    -> ignore

* { obj_krugg_investigation_started } "Heard about Krugg's snotlings?" # GUIDE case_closed
    -> ignore

// actions
* { wendy_respect_score == 0 } [Sit down]You sit down beside the fire. # GUIDE introduce_wendy/start
    -> sit_down
* { wendy_respect_score == 1 } [Show your choppa] # GUIDE introduce_wendy/1
    -> show_choppa
* { wendy_respect_score == 2 } [Take some food] You reach out and take a charred leg from the fire. # GUIDE introduce_wendy/2
    -> meet_the_meat

* { wendy_respect_score == 3 } "What's your name?" # GUIDE introduce_wendy/end
    -> introduce
+ { wendy_annoyed == 3   && wendy_respect_score < 3} "What's your name?" # GUIDE death/wendy_annoyed
+ { wendy_annoyed < 3 && wendy_respect_score < 3 } "What's your name?"
    -> ignore
* { persist_wendy_failed_hunts > 0 } "Let's hunt somefin[."]," you suggest.
    -> suggest_hunting
// TODO change up the text here (but share a trophy)
+ "Felicitations of the day, good sir!" [] you say cheerfully. # GUIDE death/wendy_weird
    -> are_you_weird
+ [Leave { obj_met_wendy:Wendy|the ork} alone] You leave  { obj_met_wendy:Wendy|the ork} alone.
    -> main

= wendy_menu

+ "Wanna play cards?" # GUIDE found_money/start
    -> play_cards
+ "Wanna go huntin'?"[] you ask. # GUIDE woo_wendy/start way_of_wendy/start
        -> suggest_hunting 
* { obj_krugg_investigation_started } "Heard about Krugg's snotlings?" # GUIDE case_closed
    "Don't know, don't care," Wendy growls.

    "Dat ork been touched by da warp. What he does is da gods business now."
    ~ obj_clue_wendy = true
    -> wendy_menu

+ [Leave Wendy alone] You leave Wendy alone.
    -> main

= introduce

"Wendy," he says, chewing.

~ obj_met_wendy = true
# TROPHY: introduce_wendy
~ trophy_introduce_wendy++

* "Good ta meet ya."
    -> wendy_menu
* [Grunt]You grunt and nod your head.
    -> wendy_menu
* "Wendy! Ha! Dat's a girly name for a big tuff ork!"
    -> upset_wendy

= show_choppa

~wendy_respect_score++

Ignoring the ork, you draw a knife from your belt.

You notice the ork's gaze following your blade.

* "Dis is Clara."
    ~ knife_name = "Clara"
* "Dis is Stabba."
    ~ knife_name = "Stabba"
* "Dis is Julius."
    ~ knife_name = "Julius"

- <> you say.

The ork's eyes widen as your blade glints in the light.

"Not bad," he admits.

Suddenly, somehow, a blade has appeared in the orks own hand. It's a bulky, cumbersome thing - but you have little doubt that the edge is very keen.

"I call 'er 'Poppet'", he whispers.

You see a flash and suddenly the knife has vanished, as if nothing ever happened.

-> root_menu

= upset_wendy

// TODO I've rewritten this thing to put wendy into an ignore state, but wound't he
// just kill you? In many ways ignoring is more powerful.
// Can't quite decide what I want to do with this.
// if it's a death story, I can add a good guide icon
// doesn't work the same for "upset" really. And I don't want a trophy for this?

Wendy gawps at you, eyes wide.

"W... wot?" he stutters.

 * "You 'erd[."]," you grunt.
 * "Nah just kiddin'[."]", you laugh.
 * "Oh my god I am so sorry - I honestly didn't mean any offence!"
    // TODO I probably want a bespoke death here
     -> are_you_weird

- "No need ta be rude 'bout a fella's name," Wendy sniffs.

~ wendy_disappointed = true

"If dat's what you wanna do, mebbe you should go bovver someone else. I ent interested."

The big ork turns his back to you and returns to his food, ignoring you.

-> go_to_main

= play_cards

"Sure fing," Wendy says, grinning darkly. "I could use some easy money."

+ [Play] Wendy deals the cards.

- He's a dreadful player, with no head for numbers and no comprehension for the rules.

You are struck with a sudden sense that, in an alternate universe where things were better, the game would be a long, winding, hilarious adventure - stuffed with all the wit and danger of a Nickleback hunt.

+ [But...] # ACQUIRE money # GUIDE found_money/end

But in this universe, the game is over quickly and you win a fist full of coins.

~ has_money = true
~ trophy_found_money++
# TROPHY found_money

 -> start

= suggest_hunting

"Never a bad time for a hunt," Wendy says.

"Dis could take a while. You ready?"

+ [Let's hunt some bugz] # GUIDE woo_wendy/begin way_of_wendy/begin
    -> start_hunting
+ [Maybe later]
    -> main

= start_hunting

"Look," Wendy says, scratching his chin. "If dis is gonna work, you gotta do what I say. We do this Wendy's way, or we get ded."

{ persist_wendy_failed_hunts > 1:
    "I've been out wiv some real idiots lately," he adds.
 }

+ [Sure]You nod. # GUIDE way_of_wendy/sure woo_wendy/sure
+ [No deal]"Unacceptable," you say.

    "Fine," Wendy shrugs. "I'll go meself, den."

    You leave Wendy to prepare and head back into  camp.
    -> go_to_main

-

"Well, dere's only two fings worf huntin around here," Wendy says, scratching behind his ear. "Nicklebacks, wot are good eatin', and worms, wot are a bit deadly."

* [Hunt Nicklebacks]You suggest hunting Nicklebacks.
    "Suit yerself," Wendy says, "But I got a taste for worm meat."
* [Hunt... Worms?]You suggest hunting Worms.
    "Ha!" Wendy grunts. "Fat chance. You've not got the chin fer it."

- Wendy climbs to his feet and reaches behind a pile of rocks. To your surprise, he pulls out a large wooden bow.

"Can't go 'untin wivvout this," he says, looking lovingly across the curve of the bow.

* [Agree]You couldn't agree more, you say.
* [Disagree] "Dat doesn't seem very orky," you say.

Wendy grunts.

- Wendy walks across to a huge, rusting metal storage box. He holds the bow up, drops one end deep into a hole on top of the box, and twists with two hands.

The lid of the storage box pops open. 

"'Unter's best friend!" Wendy beams, throwing the bow aside.

// TODO better flow? This is lazy
* [Continue]

-Wendy leans into the storage box pulls out grenade after grenade, stuffing them into a canvas sack.

"Reckon that'll do?" he says, looking at the overflowing bag of grenades.

* "For you, maybe[."]," you say. # GUIDE way_of_wendy/extra_nades
    ~ wendy_way_score++
    Wendy grins, grabs a second sack, fills it with grenades, and hands it to you.
* "That's plenty[."]," you say.
    Wendy grunts and tosses two more grenades at you.

    You hastily catch them and stuff them into your rucksack.

- Finally, Wendy pulls a pair of bulky machine guns out of the storage box and hands one to you.

# IMAGE: hunting

"If yer krafty enuff," he says gravely, "You won't even need this."

* [Take the gun]

- You take the gun from Wendy and give it a once-over.

It's a bulky heavy-machine gun, totally unsuited to hunting. You shrug the strapping over your shoulders and let it hang across your back.

Wendy lumbers out of the camp, rattling under the weight of his grenades and oversized gun. You follow behind.

"Remember," Wendy says. "You gotta do exactly what I say, or we'ez ded."

* [To glory!] -> find_tracks

= find_tracks
{ once:
    -You wander the desolate badlands for about an hour before Wendy breaks the silence.
    
    "Look 'ere! Nickleback tracks!"

    Wendy kneels down next to a series of imprints on the red clay.

    "Hunting pack, goin' dat way," Wendy nods along the tracks.

    "So if we go da ovver way, we find da nest." Wendy's eyes gleam.

    "Wot you reckon?"

    - "Hunters ain't gonna hunt in dere own nest, are dey?" Wendy grunts. "Stands to reason."

    - Wendy's brow furrows.

    "But den, da tracks would go da ovver way, wouldn't dey?"

    - "Fanks," Wendy says.

    - "We go to da nest," Wendy says. "More meat fer us."
}
{ once:
    - * "What makes you think they came FROM the nest?"[] you say. -> find_tracks
    - * "Maybe they've finished the hunt and are coming back?"[] you say. -> find_tracks
    - * "I don't think you've thought this through[."]," you say. -> find_tracks
    - * "What do you think?" you ask. -> find_tracks
}

* [Chase the pack] You walk along the tracks, in search of the pack. Wendy gives a disappointed sigh. # GUIDE woo_wendy/patrol
    -> follow_tracks
* [Go to the nest] You walk away from the tracks, towards the nest. # GUIDE woo_wendy way_of_wendy/nest
    -> reverse_tracks

= follow_tracks

{ stopping: 
    -You walk in silence for a short time, but soon come across a pair of Nicklebacks scratching at the clay.

    Wendy takes cover behind a boulder. You join him and consider your options.

    You are surrounded by dense thickets of brambles and shrubbery. Large boulders litter the hard-baked clay of the badlands.

    "Gun 'em down, nice and quick," Wendy drawls.
    - <>
}

* [Examine the Nicklebacks]
    -> examine_nickleback -> follow_tracks

* [Draw your gun] # GUIDE death/wendy_hunt_gun
    You nod at Wendy and sling the heavy machine-gun from your shoulder, resting it on the boulder.

    Wendy stands and plants his feet in the clay, holding his own gun ready.

    You peer quickly down the gun before Wendy breaks the element of surprise.

    * * [Take aim] You draw a bead on the leading Nickleback and squeeze the trigger.

    The gun kicks back against your shoulder, but fires true. The Nickleback flies backwards under the impact, skidding along the clay with a screech.

    Wendy opens fire a moment later, spewing bullets into the Nicklebacks, the clay, the dense orange rocks.

    You hear a skittering sound behind you.

    * * * [Turn around] You whip around to see three Nicklebacks sprinting through the thicket towards you.

    You ready your gun - but the first Nickleback is upon you in moments, knocking you to the ground. It tears at your skin and bites deep into your flesh.
    
    You hear Wendy's machine gun burst back into life as the Nickleback bites into your neck.
    -> death
* [Draw your knife] You shake your head and slowly draw your knife. # GUIDE woo_wendy
    // TODO this is a bit rushed
    Wendy grunts, drops his machine gun, and pulls out his own blade.

    "Fine," Wendy spits. "We do dis da hard way."

    * * [Stalk a Nickleback] You creep through a thicket towards the of Nicklebacks.

    They don't notice your approach.

    "Now!" Wendy hisses, preparing to charge.

    * * * [Charge] You charge towards the first Nickleback, feet pounding on the clay, Wendy by your side. # GUIDE death/wendy_hunt_charge

        The Nickleback whips around, screeching, as you leap into it.

        You roll on the ground, stabbing and scratching as the thing frantically claws at your skin beneath you. You yell out in pain and struggle against it.

        You distantly hear Wendy's grunts and the dying rasps of his Nickleback.

        * * * * [Go for the jugular] You drive your knife into the Nickleback's throat. It splutters and falls still.

        You roll onto your back, feeling your skin burn from the Nickleback's claws.

        Then a chorus of screeches rises from the scrub around you.

        * * * * * [Get up] You drag yourself to your feet as two more Nicklebacks pounce upon you.

        Wendy starts blasting into the bushes. You hear a grenade go off as jaws clamp around your own throat, and the world falls silent.

        -> death

    * * * [Move Closer] You shake your head and creep closer to the Nicklebacks. # GUIDE woo_wendy
        // TODO this is a bit rushed

        You get to an arm's width from the beast, then pounce onto it, driving {knife_name} deep into its neck.

        The Nickleback wheezes and flails on the floor. Its companion rears up, hissing, before Wendy dives on it, stabbing frantically into its hide.

        * * * * [Twist the knife] You twist the blade and the Nickleback lies dead.

        After some panting and wheezing, Wendy kneels up above his own dead Nickleback.

        * * * * * [Get out of there] "Well, dere we go," Wendy grunts, heaving the dead beast onto his shoulder. # GUIDE woo_wendy/end # ACQUIRE respect

        "Time fer lunch."

        You stand up, collecting up your own Nickleback, and head back towards the camp.

        -> ending_good

= reverse_tracks

{ stopping:

    -As you follow the tracks to their source, Wendy loudly tells stories of past hunts and great victories. You have never seen the stoic ork be so animated - he raves and rambles, his voice booming across the open badlands.

    You notice a gathering swarm of bird-like creatures wheeling overhead.
    - Wendy continues to rant loudly about his greatness.
}

* "Are those birds following us?"[] you ask.
    "S'just Razorwings," Wendy shrugs, but his eye lingers on the gathering swarm.

    // * * "Don't bats only come out at night?"[] you ask.
    //     "Dey aint real bats. They just call demselves that to be confusin. Itsa surfival mechanism. Now, as I was saying..."
        -> reverse_tracks
    * * [That seems fine]
        -> reverse_tracks
* [Tell him to be quiet] "Wendy, will you keep it down?" you hiss. "You'll scare the game off!"
    Wendy grumbles, but shouts a little less loudly.
// TODO is this broke?
* [Tell your own stories] # GUIDE way_of_wendy/stories
    ~ wendy_way_score++
    Not wanting to be outdone, you start bragging about your own hunting prowess, loudly boasting about the beasts you've killed, battles you've won, and lovers you've taken.

    It's all lies, of course, but you enjoy the banter.

-You keep walking until you come upon the Nickleback nest.

* [Examine the nest]

- The Nickleback nest spreads among rocky outcroppings and spiny dry shrubs.

A few Nicklebacks graze and pace around the area, unaware of your presence.

 -> examine_nickleback ->

You watch as one skitters down into a tunnel.

* [Discuss tactics]

- "Ok, here's da plan," Wendy says, crouching down in the clay.

"First off, we kill da Nicklebacks," he says.

Wendy looks at you expectantly.

* "...Yes?"[] You prompt.
* "Seems simple enough[."]," you say.
* "Great plan, Sun Tzu[."]," you scoff.

- "Good plan, init?" Wendy grunts.

Wendy pulls out a handful of grenades from the sack and hurls them out into the badlands.

They land heavily on the clay, causing a few Nicklebacks to startle.

-> weapon_choice

= weapon_choice

"Let's do this real sneaky like," he hisses at you.

* [Draw your knife] # GUIDE death/wendy_hunt_knife
    You rest the heavy gun on the clay and draw {knife_name} from its holster, savouring the glint of light which flashes across it.
    -> knife_fight
* [Ready your gun] # GUIDE woo_wendy/gun
    -> gun_fight 
* [Grab a grenade] # GUIDE way_of_wendy/nade
    -> nade_fight

= knife_fight

// wendy told you to be sneaky. He means to throw grenades into the pack,
// but a sensible player would interpret it to mean attempt actual stealth
// Following wendy's advice = bad news, so this ends in death
{ stopping:
    - Wendy's grenades explode, kicking up showers of red dust against the badlands.

    This stirs the Nicklebacks up into panic. They flail about in the confusion, joined by their spiny bretheren pouring out of the tunnels.

    Wendy readies his machine gun and starts firing into the dust and Nicklebacks.
    - <>
}

* "I thought you were going to be sneaky?!"[] you shout.
    "I was!" Wendy yells above the clackclackclack of the gun.
    -> knife_fight
* [Move in for a kill]
    You inch forward, keeping low, as Wendy's bullets zip overhead.

    You start to think they're a little too close for comfort when one tears across your back. You scream in pain and fall to the hard clay.

    You hear a high-pitched screeching sound just in front of you.

    * * [Look up] You jerk your head up just in time to see a purple Nickleback flying through the air towards you.

    Your screams cut through the sound of Wendy's gunfire.

    -> failed_hunt
* [Stay hidden]
    You wait behind a rock as Wendy fires at anything that moves.

    The Nicklebacks start to gather themselves and surge towards the rattle and clack of Wendy's gun.

    You grip {knife_name}, but there are too many for Nicklebacks for you to fight at once.

    "Charge!" Wendy screams over the deep rattle of his gun.

    * * [Charge]
        You scream and hurl yourself into the nearest Nickleback.

        You roll and tumble across the clay, feeling the Nickleback's claws tear your skin as you hack wildly with your knife.

        Somehow the beast ends up on top of you.
    
        * * * [Stab it! Stab it!!]

            You plunge your blade into the Nickleback's flank as it closes its jaws around your skull.

            -> failed_hunt
        // TODO If you have armour, you survive the first attack

    * * [Stay hidden]
        ~ obj_wendy_dead = true

        You stay low as the Nicklebacks charge towards Wendy.

        You watch for a while as he battles the beasts. He kills two, three, four before he is brought to the ground.

        You wait until the Razorwings arrive to pick clean Wendy's carcass, then slip away into the badlands.

        -> go_to_main

= gun_fight

// Survive to see the wyrm, but not enough wendy points to kill it
"Safety off!" you shout, hefting your gun. "Weapons hot!"

Wendy gives you a funny look.

"Safety?" he grunts.

You open fire as Wendy's grenades explode - blasting up great reddish clouds of dust and clay.

* [Shoot the Nicklebacks]

- Wendy's grenades continue to erupt. Soon you can barely see the Nicklebacks in the red fog.

Wendy plants his feet and opens fire by your side.

You see some creep out of tunnels and take aim, blasting them into bloody pieces as they emerge.

The Nicklebacks work out where the threat is coming from and start to surge towards you.

The gun is hot and heavy in your hands.

* [Keep firing] # GUIDE woo_wendy/firing
    You focus on your fire on the Nicklebacks as they charge towards you. You drop one, then another, then a third - bullets fizzing through the air, pocking the clay.

    -> razorwings

* [Take a break] # GUIDE death/wendy_hunt_ceasefire
    You release your finger from the trigger and take a breath. Your whole body relaxes.

    "Wot you doin'?!" Wendy yells in horror.

    Wendy's gunfire isn't enough to hold the Nicklebacks at bay.
    
    * * [Resume Fire]

    You open fire once more, but the tide of Nicklebacks quickly overwhelms both of you. You fall to the ground under the slashing of claws and the gnashing of jaws.

    -> failed_hunt


= nade_fight

~ wendy_way_score++
VAR is_firing = false

You dig out your own grenades and hurl them out among the Nicklebacks.

"Oh dat's real sneaky," Wendy grunts in appreciation.

Explosions shudder across the badlands in a sudden frenzy of violence. Great spurts of dust and clay erupt into the air, creating a red fuge around the nest.

Nicklebacks run and scream in confusion and panic, some vanishing into the tunnels, others pouring out in confusion, only to retreat as another fireball bursts into the air.

Wendy starts firing his machine gun into the fray.

"Choose yer targets!" Wendy yells.

* [Let 'em have it]
    ~ is_firing = true
    You heft your own gun and start blasting at the Nicklebacks. The recoil sends your shots wide but you quickly bring it under control.

* [Hold Fire] # GUIDE death/wendy_hunt_hold_fire
    You hold off, waiting for the perfect moment.

- Some of the smarter beasts gather themselves and charge in your direction.

{ is_firing:
    You and Wendy are able to gun down the oncoming Nicklebacks. Wendy laughs as they drop to the clay.

    * Dakkadakkadakka[!] your gun roars.

    You grit your teeth and keep firing.

    -> razorwings
- else:
    -> death_by_Nicklebacks1
}

= death_by_Nicklebacks1
Wendy guns down two of the leading Nicklebacks.
    
* [Open fire]

- You take aim for a third, but the tremendous recoil from the gun drags your shot wide.

The Nickleback pounces onto you, throwing you to the ground.

// TODO - armor
* [Fight it off]
    { has_armour:
        The Nickleback's claws scratch against your armour.
        
        You try to throw the beast off you, but something bites into your legs. You scream in pain.
     -else:
        You flail uselessly at the Nickleback as it tears at your flesh.
    }
* [Cry for help]
    You shout to Wendy for help, but your cry turns into a scream as the Nickleback rends the flesh from your legs.

-Wendy backs off, still firing. Another Nickleback piles on to you.

* "Wendy!"[]
* "Mother!"[]
* "Bastards!"[]

- <> you scream.

Wendy hurls one last grenade in your direction, then flees.

-> failed_hunt

= razorwings

VAR target = ""

Then the Razorwings descend.

Drawn by the scent of charred Nicklebacks,
- { wendy_way_score > 2:
        <> a thick swarm of Razorwings swoops down from the sky. They swoop and bite and scratch at the Nicklebacks, creating a furious melee.
    -else:
        <> a scattering of Razorwings sweep down to feast on the charred remains Nicklebacks.
}

The Nicklebacks turn to face them, snatching them out of the skies and pouncing on them as they pick at cadavers.

* [Shoot the Nicklebacks]  
    ~target = "Nicklebacks"
* [Shoot the Razorwings]
    ~target = "Razorwings"

-The machine gun rattles and spits and barks in your hands as it fires out across the badlands, cutting down {target}. The beasts and birds battle each other in a thick haze of dust and smoke.

Wendy laughs manically and kicks more grenades into the tumult.

// Only get this if there are enough razorwingss
{ wendy_way_score > 2:
    An eerie moment of calm falls upon the scene as a shadow passes over the land.

    * [Look up]
        -> wyrm_attack
- else:
    You notice something huge drifting high in the sky on the horizon, but it wheels slowly out of sight.

    * [Fight on]

    You fight for some time, feet planted on the clay, guns firing, beasts screaming.

    Soon the smoke thins and Wendy stop firing.

    -> battle_over
}

= battle_over

The badlands fall quiet as clouds of dust and smoke drift over the ravaged Nickleback nest.

* [Observe the battlefield]

-The Nicklebacks are dead or hiding deep underground. A couple of Razorwings pick at the carcasses littering the clay. A few more circle overhead.

"No worms, today," Wendy sighs, gazing up at the sky and shouldering his gun.

* [Lower your weapon] You relax the heavy weight of your own gun and rest it on the ground.

- Wendy walks over to the smoking body of a Nickleback, tears a limb from it, and bites into the charred meat.

"Perfec'!" he growls around his food.

* [Take your rewards] # GUIDE woo_wendy/end
    You clap Wendy on the shoulder. He grunts happily and takes another bite of gamey Nickleback meat.

    You fill the empty canvas sacks with all the Nickleback you can and head back to Wendy's camp.

    -> ending_good

= wyrm_attack

You look up to see a serpentine shape with wide leathery wings passing over your head. It wheels around in a slow arc and lets out a piercing scream.

* [Wait, that's a WYRM?!] "Oh my god you're not hunting a worm, you're hunting a WYRM!" you gasp.
    Wendy gives you a funny look.

    "Yes? Day's what I said?" he growls.
* "We've got company, again."[] you breathe.

- "Just keep firin'!" Wendy barks.

The wyrm feasts on the cloud of Razorwings, closing its huge jaws around packs of them as it soars through the air like a whale filtering kelp.

* [Shoot the Nicklebacks] # GUIDE woo_wendy/nicklebacks
    -> wyrm_landing
* [Shoot the wyrm] # GUIDE woo_wendy/wyrm
    -> wyrm_landing
* [Shoot the Razorwings] # GUIDE woo_wendy/razorwings
    -> wyrm_landing
* { wendy_way_score > 2 } Throw grenades at everything that moves # GUIDE way_of_wendy/throw_nades
    -> grenade_all_the_things

= grenade_all_the_things

You throw the last your grenades out across the clay. They erupt, joining the cacophany, launching a shower of Nicklebacks into the air. Razorwings dive into the chaos, biting and tearing at stucklebuck flesh.

The wyrm arcs around in a slow circle, then dives towards the ground.

// Ok he's right about this one. Maybe think about this some more?
"You might wanna step back," Wendy breathes, his weapon falling silent.

* [Step back] You take a careful step backwards.

- The giant wyrm crashes into the clay, devouring half a dozen razorbacks as it descends, claws gouging two trenches in its wake.

The ground, weakened by the assault from Wendy's explosives, gives way under the wyrm's weight. It collapes into the maze of Nickleback tunnels below the surface. It flails and screams and tries to pull itself from the clay, but as it whips its wings it only falls deeper into the ground.

// TODO
* [Continue]

- Enraged Nicklebacks jump into the wyrm and scratch at its thick skin. Razorwings swoop into the confusion, a howling cloud of rage, biting and clawing at the giant monster.

The wyrm screams and desperately tries to pull itself from the ground.

Wendy fixes a grenade below the barrel of his gun, and hands his last one to you.

* [Take the grenade] You fix the grenade to your own gun and take aim at the wyrm.

- You both fire at the same time, the grenades arcing through the air. 

Wendy's crashes into the side of the wyrm's head and explodes in a fierce fireball.

* [Watch your grenade] 

- Your own grenade flies straight into the wyrm's screaming mouth. It vanishes from view for a moment, then bursts the wyrm's head like a grim firework.

The wyrm's lifeless body collapses to the clay.

All falls silent, as if the badlands themselves draw in a deep breath.

* "That was ridiculous!"[] you scoff. # ACQUIRE respect
    "Don't go makin' up words," Wendy grunts. "No need fer it."
* "Did you SEE that?"[] you cry, pointing excitedly towards the dead wyrm. # ACQUIRE respect
    "Not bad fer a newbie," Wendy grunts.
* "You missed," you grunt, leaning on your gun. # ACQUIRE respect
    Wendy looks at you for a moment, then laughs and claps you on the shoulder.

-Wendy shoulders his gun and marches towards the wyrm.

"Dat's how a really kunning ork hunts," he grunts.

-> ending_way_of_wendy

= wyrm_landing

The wyrm arcs around, then dives towards the ground.

// Ok he's right about this one. Maybe think about this some more?
"You might wanna step back," Wendy breathes, his weapon falling silent.

* [Step back] You take a careful step backwards.

- The giant wyrm crashes into the clay, devouring half a dozen razorbacks as it descends, claws gouging two trenches in its wake.

It crashes into the badlands, kicking up a huge cloud of dust. Its great jaws scoop up Nicklebacks and swallows them down whole.

"Uh oh," Wendy growls. "Not krafty enough!"

He brandishes his knife. 

"Guess we gotta finish this the old fashioned way."

* "Sod that!"[] you say, dropping your gear and backpedalling. # GUIDE woo_wendy/end # ACQUIRE respect
    Wendy grunts and sheathes his blade.

    "Not as dumb as you look," he says.

    You both flee the nesting grounds as the wyrm fills its belly.

    -> ending_good
* "Let's cut us some wyrm!"[] you roar. # GUIDE death/wendy_wyrm

Wendy mutters something as you draw your knife and charge towards the huge wyrm.

Razorwings fly in a thick cloud around the monster, while Nicklebacks buzz and scamper around, trying to defend their nest.

Your feet hammer across the clay - but Wendy's footsteps are conspicuously absent.

* * [Keep running] You keep running towards the wyrm.
    You dodge swooping Razorwings and a dazed Nickleback and leap at the wyrm's leg, {knife_name} oustretched.

    Your blade skids across the wyrm's thick scales. You bounce onto the clay, landing hard and gasping for breath.

    // TODO armor
    * * * [Get up] As you pull yourself to your feet, the wyrm lashes out with a claw, skewering you in several places.

    -> failed_hunt
* * [Look at Wendy] You twist your head, looking for Wendy.

    You turn around and see him walking off into badlands, knife sheathed, both guns slung over his shoulder.

    His posture speaks of disappointment.

    You trip and fall hard onto the clay.

    * * * [Get up]

    As you try and pull yourself to your feet, a swarm of Razorwings flutters around you, pecking at your flesh and eyes.

    You try and fight them off but the pain is overwhelming.

    -> failed_hunt

= examine_nickleback

The Nicklebacks are spiny, reptilian things, with grey skin and a coating of thick purple-grey carapace shards and spines. They stand on four legs, a little less than a metre tall.

/* tunnel */
->->

= failed_hunt

~ persist_wendy_failed_hunts++
-> death

= ending_good

Back at camp, Wendy claps you on the shoulder.

"Ya did good," he grunts. "Next time you on 'unt sumfink, gimmie a shout.'

# TROPHY: woo_wendy
~ trophy_woo_wendy++
~ obj_orks_beaten += 1
~ wendy_done = true
~ wendy_respect = true
// TODO need to actually communicate this to the player
~ has_meat = true 

-> go_to_main

= ending_way_of_wendy

~ obj_orks_beaten += 1
~ wendy_done = true
~ wendy_respect = true
~ trophy_way_of_wendy++
# TROPHY: way_of_wendy

Wendy hacks off some strips of flesh from the wyrm and stuffs them into his canvas bags.

* [Cut some meat] # ACQUIRE meat # GUIDE found_meat/end
    ~ has_meat = true
    ~ trophy_found_meat++
    # TROPHY: found_meat
    You draw {knife_name} and help Wendy carve flesh from the wyrm.

    "Ya know," Wendy grunts after a time. "I don't fink I've ever seen anyone hunt like dat."
    
    He shoots you a glance and says:
    
    "Yer alright fer a weirdy."

    You thank him and he grunts awkwardly.

    Soon you finish carving the meat and head back to camp to prepare a meal.

    -> go_to_main
* [Just go home] 
    -> go_to_main
