/*
  Daniel is an ork raised in a French kitchen on SOME PLANET. He pretends to be a brutish thugg but it's not real. Speaking to him normally will quickly encourage him to drop his facade.
  
  Maybe you can speak to the other orks about Sharptoof?

  Give Squig to sharptoof. One of the reasons he's weird is that he keeps it as a pet,
  rather than eating it. He refuses to add it to stew.
*/

VAR persist_sharptoof_likes_cooking = false
VAR persist_sharptoof_likes_mushrooms = false
VAR persist_wendy_likes_squig = false
VAR persist_sharptoof_dislikes_orks = false
VAR persist_sharptoof_dislikes_betrayal = false
VAR persist_sharptoof_dislikes_meat = false

VAR squig_cooking = false
VAR squig_shortcut = false
VAR daniel_score = 0
VAR is_second_visit = false
VAR daniel_says_thanks = false
VAR daniel_naive_jab = true

== function status_sharptoof ==

{
    -obj_met_sharptoof:
        ~ return "Sharptoof stirs his stew"
    -else:
        ~ return "An ork stirs a huge, bubbling cauldron"
}

== Sharptoof

= profile

\ {met_daniel: DANIEL|SHARPTOOF}

Likes:
    \* { -persist_sharptoof_likes_cooking:
        <> Cookin'
        -else: <> ???
        }
    \* { -persist_sharptoof_likes_mushrooms:
    <> Shrooms
    -else: <> ???
    }

Dislikes:
\* { -persist_sharptoof_dislikes_orks:
        <> Orks
        -else: <> ???
        }
\* { -persist_sharptoof_dislikes_betrayal:
        <> Betrayal
        -else: <> ???
        }

+ [<< BACK] -> main

= start_debug

~obj_met_skullkrusha = true
-> start

= start

- { 
    -met_daniel:
        -> daniel_visit
    -first_visit > 0:
        -> other_visits
    - else:
        -> first_visit
}

= first_visit

VAR whats_for_grub = 0

An ork stirs a charred cauldron over a spitting fire. He is dressed in a crude homage to a chef's whites, with a white hat and a belt adorned with pouches and a dangling chain of mushrooms.

A fat squig sleeps at his feet.

The ork pulls out his ladle, sniffs delicately at it, then growls. He rummages in the pockets lining his belt, then tips a white dust into the cauldron, glittering as it falls into the bubbling greenish goop.

The ork takes another sniff of the goop and smiles happily - then scowls when he notices you looking.

// TODO I want the player to learn by now that being polite gets you killed or ignored
// Until now, anyway. Have I done enough to set this up?
+ "Hello."
    "Wot you lookin' at?" he growls, then cracks his ladle against the cauldron.
+ "What's for grub, ya poncy runt?"[], you ask.
    ~ whats_for_grub++

You see a strange shadow pass over the ork before he collects himself.

"Oi! No-one speaks to Sharptoof like dat!" he roars, brandishing his ladle and stomping aggressively on the floor. 

- "I'm da meenest, tuffest, most brutal chef on da whole stinkin' planet!"
~ obj_met_sharptoof = true

+ "Dat's what you say."
    Sharptoof grunts and raps on the cauldron with his ladle.
    
    "Grub's nearly ready!" he beams.
    -> talk_about_stew
+ "{ whats_for_grub == 0:Whatever. What's for grub?|I said, what's fer grub?}"
    Sharptoof grunts and raps on the cauldron with his ladle.
    -> talk_about_stew
+ "Alright my good fellow, I take your point. No need to get excited." # GUIDE woo_sharptoof/start-a
    -> break_cover
* { obj_met_skullkrusha } "You got some grub for da boss?" # ACQUIRE stew 
    -> feed_da_boss

// TODO if you know Daniel's secret, we have to have something else here.
// Is it too late to get the ending? I don't think so.
= other_visits

-> sharptoof_menu

= sharptoof_menu
    "You agin?" the ork snarls.
    
+ { squig_cooking == false } "'Ow about { try_food:more of|some of} dat grub?"
    Sharptoof growls, dips his ladle into the cauldron, and takes a big slurping sip.
    
    "Aah!" he eays, satisfied, his rage subsiding.
    
    -> talk_about_stew
+ { insult_food } "My good ork, I simply couldn't rest without apologising for my earlier behaviour."
    -> break_cover
* { obj_met_skullkrusha } { squig_cooking == false } "You got some grub for da boss?"  # ACQUIRE stew 
    -> feed_da_boss
* { has_meat } { squig_cooking == false } "Can you put dis Nickleback meat in da stew?" # GUIDE found_meat_stew/start
    -> cook_squig
* "What's wiv da squig?"
    -> meet_squig    
* { squig_cooking } "Dat squig surprise ready?"[] you ask. # ACQUIRE meat_stew # GUIDE found_meat_stew/end
    ~ has_meat_stew = true
    ~ trophy_found_meat_stew++
    # TROPHY: found_meat_stew

    "Yer," says Sharptoof. "Here ya go."
    
    He hands you a steaming bowl of actual squig surprise.

    * * [Go back]
        -> main
* { obj_krugg_investigation_started } "What's up with Krugg's snotlings?"[] you ask. # GUIDE case_closed
    -> investigation_sharptoof

* { obj_met_skullkrusha } { !feed_da_boss } "You got some grub for da boss?" # ACQUIRE stew 
    -> feed_da_boss

// TODO maybe an option to chat him up here?
+ "Forget it."
    You leave the grumpy ork in peace.
    -> go_to_main

= daniel_visit

~is_second_visit = true

You approach Daniel at his cauldron. He shares a quick, warm smile with you before resuming his cover. He swears and grumbles as he stirs the stew.

-> daniel_menu

= talk_about_stew
{ stopping:
    - "I got a loverly squig surprise stew."

    ~ persist_sharptoof_likes_cooking = true

    "Needs a dab more salt, though," he says under his breath, eyeing the stew suspiciously.
- <>
}

* { !squig_shortcut } "Squig surprise? Splendid! A celebration of gastronomic pragmatism!" # GUIDE woo_sharptoof/start-b
    -> break_cover
* "What's da surprise?"[] you ask.
    ~ squig_shortcut = true
    "Well," he says, puffing his chest out. "I can't put actual squig in it, can I? It'll upset poor Squig 'ere'."

    Sharptoof looks longingly at the little round squig sleeping at his ankles.

    "So I use mushrooms instead."
    -> talk_about_stew
* { squig_shortcut } "What I'd give for a few escargot and a splash of chardonnay!" # GUIDE woo_sharptoof/start-c
    -> break_cover 
* { has_meat } "I've got some Nickleback meat for yer[."]," you say. # REQUIRE meat # GUIDE found_meat_stew/start
    -> cook_squig
+ "I'll try some then, fanks."
    -> try_food
+ "Sounds rubbish." -> insult_food

= try_food

Sharptoof grabs a dirty tin bowl and spoons out some gooey clumps of greenish stuff into it.

You tenatively take a bite. It tastes green, with just a subtle hint of beige.

* "Fanks chef."
    Sharptoof nods at you and returns his attention back to the cauldron.
    
    You finish your food and walk away.

    * * [Go back]
        -> main
* "Well I'll be - considering the extraordinary constraints your are operating under this dish is, well, quite extraordinary!" # GUIDE woo_sharptoof/start-d
    -> break_cover

-> DONE

= insult_food

"Then go cook yer own grub, you mangy skag!"

Sharptoof roars and hurls his ladle at you.

You take the hint and walk away.
-> go_to_main

= cook_squig

# USE_ITEM: meat

"Actual meat?" Sharptoof looks astonished.

"Now da surprise is dere's actually some squig in it!" he cries, gleefully, then laughs maniacally for a matter of minutes.

"Sure fing," he says, calming down a bit. "Give it 'ere."

* [Give him the meat] You hand over the meat.

- Sharptoof inspects the hunk of Nickleback meat, then tosses it into the stew.

"Come back in a bit," he says.

~ squig_cooking = true

* [Go back]
    -> main

= break_cover

Sharptoof stares at you, his jaw hanging open, eyes wide.

"You... you..." he stammers.

"Oh my good lord, you're not like the rest!"

* "Glarg?"
* "Wait, what?"
* "Pardon?"

- Sharptoof leans in and claps a meaty hand on your shoulder.

"You're like me, aren't you?" he hisses, dropping his accent and speaking clearly.

"We don't belong here. We're not like these... savages."

* "No, we're not[."]," you say, thoughtfully.
    "I knew it!" he whispers exitedly. "There had to be others, there just had to be!"
* "Oo you callin savage!"[] you say, puffing out your chest.
    "Don't worry," he says, looking over his shoulder before meeting your gaze. "This is a safe space, friend. I won't tell anyone."
* [Say nothing]

- "My name isn't <i>Sharptoof</i>," he spits out the name with a shudder. "It's Daniel."

~ met_daniel = true

* "Pleased to meet you, Daniel." # GUIDE woo_sharptoof/end
     ~ sharptoof_done = true
     ~ obj_orks_beaten++
     ~ trophy_woo_sharptoof++
     # TROPHY: woo_sharptoof
     
* "You wot? Why you gone all poncy?" # GUIDE death/sharptoof_rage
    "Wait, you mean you're... oh curses. I won't blow my cover again!"
    -> killed_by_daniel
    
- "Likewise! Oh, it's so nice to find a friend out here, I can't even tell you!"

"Come sit. Sit!" Sharptoof beckons to a couple of rocks by the cauldron. You make yourself comfortable.

-> daniel_menu

=daniel_menu

* "Tell me about yourself[."]," you say.
    -> daniel_backstory
* "How do you survive out here?"[] you ask.
    -> daniel_survival
* { daniel_score > 1 } "You should stick up for yourself[."]," you say. "Be yourself."
    -> daniel_fight
* [Enough of this charade!] "Freak!", you shout. "Abomination!" # GUIDE death/sharptoof_rage2
    "Betrayal!" Daniel roars.
    -> killed_by_daniel
* { daniel_score > 0 } [Explain the curse of the Eldar Witch that brought you here.] # GUIDE something_more/start
    -> explain_curse
* { !investigation_sharptoof } { obj_krugg_investigation_started } "What's up with Krugg's snotlings?"[] you ask. # GUIDE case_closed
    -> investigation_daniel
* { !is_second_visit } "Thank you for sharing this with me, but I have to go."
    "Yes, of course. Let's keep this our little secret, eh?" Daniel winks, clapping you on the shoulder.
    
    You agree to meet after dark. Smiling, you return to the camp.
    -> go_to_main
+ { obj_met_skullkrusha } { !feed_da_boss } "Do you have anything for Skullkrusha to eat?" # ACQUIRE stew 
    -> feed_the_boss

+ { is_second_visit } [Hang out]
    You and Sharptoof sit for a time.

    You talk about life, injustice, and mushrooms until the stars twinkle overhead.

    You retire, feeling happier than you can remember.
    -> go_to_main

+ { is_second_visit } [Leave]
    -> go_to_main

= meet_squig

"Dat's me pet squig," Sharptoof mumbles. "Squig."

* "Cute. Wozzit called?"
    "Squig," Sharptoof says.
        -> daniel_menu
* "Why did you say 'squig' twice?"
    "Wot? I said it's my pet squig, Squig. It's called Squig coz it's a squig."
    
    Sharptoof stares at you.

    "You fick or somefin?"

    * * [Never mind] You sigh.
    -> daniel_menu


=daniel_survival
~ daniel_score++

"I hide," Daniel says, darkness flickering across his rough features.

"They'll never understand, you see. They'll never accept me for who I am - no-one will. Almost every living thing in the galaxy would shoot me on sight."

* "Bastards[."]
* "The universe is a cruel place[."]
* "I accept you[."]
    ~daniel_says_thanks = true

-<>," you say.

Daniel nods.

{ daniel_says_thanks: "Thanks," he says. }

"At least amongst the greenskins, I can... exist. In a way."

The ork sighs.

"I did try, for a while. To speak full sentences. To wash. To seek pleasure without hurting someone else."

* "What happened?"
    Daniel snorts.

    "Ridicule. Pain. Torture."
* "You were naive."
    ~daniel_naive_jab= true
    "Perhaps," Daniel replies. "But I had to try."

- "They call me 'poncy' and 'weird'. Sometimes they attack, but I've grown strong, and fast."

"Their words don't hurt me - but the absolute rejection does. I can't seem to harden myself to that, no matter how much I hate them."

Daniel laughs ruefully.

* "Ironic."
* "That's messed up."

-Daniel stares into the bubbling cauldron.

"At least they let me live. If I can keep out of their way and work with my mushrooms... well, it's not so bad."

-> daniel_menu

=daniel_fight
~ daniel_score++

Daniel scoffs.

"That would be suicide. Or perhaps genocide."

He shakes his head sadly.

"No, they'll never change. Their whole culture - their whole <i>nature</i> - is to be like this. Violent, intolerant, selfish, gullible."

"They don't have the intelligence, courage or empathy to ever change. You can't change the sea. You can't change the sun. You can't change ignorance when it's this thick."

"You can't change the nature of a being."

* "An individual? No. A culture? Perhaps, given time."[] you suggest.
    {
        -daniel_naive_jab: "Who's naive now?" Daniel replies.
        -else: "This sun will supernova before the Orks change their ways," Daniel replies.
    }
* "You're right."
    Daniel nods glumly.
* "I vote" for genocide."
    Daniel laughs bitterly.

- -> daniel_menu


=daniel_backstory

~ daniel_score++
"Well, I was raised in a kitchen on Terra," Daniel says excitedly.

"I was the pet of some Commissar. I escaped, of course, and was taken in by the Light Of The Empreror Bistro... for reasons I'll never understand."

Daniel looks sad for a moment.

"Those were wonderful days," he whispers. "Wonderful."

* "Go on[."]," you prompt.
* Wait[]

- "They taught me so much. Cuisine. Culture. Kindness. And the mushrooms... oh my!"

~ persist_sharptoof_likes_mushrooms = true

"When the Inquisition came they burned the place to the ground. I had nowhere to go. So I came here to my, uh, own kind."

Daniel's face twists into a grimace.

"Now I scratch out a living here, among these hateful, stupid beasts."

~ persist_sharptoof_dislikes_orks = true

-> daniel_menu

    
= explain_curse

- "I am also not what I seem, Daniel," you say.

"I was cursed by an Eldar Witch to take the form of an ork, to live and die a thousand times in this green skin."

"Truly?" Daniel gasps. "That is a dark fate indeed." 

* "Run away with me."
* "Let's leave this place."

- Daniel's eyes light up.

"Yes! We don't belong here! We can live a life, the two of us. Maybe we can be... happy?"

Daniel extends a hand to you, hope and tears glistening in his eyes.

* [Accept his hand] # GUIDE something_more/end
    ~ obj_orks_beaten ++
    ~ sharptoof_done = true
    # AUDIO: ending-good
    You hold his hand in yours, gripping it firmly, feeling the heat of his leathery green skin.
    
    You leave the camp together, walking hand-in-hand into a future uncertain, but full of hope.

    * * [Next]
        ~ trophy_something_more++
        # ENDING: elope.jpg In the grim darkness of the future, you have found something more.
        # TROPHY: something_more
        -> fin

* [Reject his hand] # GUIDE death/sharptoof_rage3
    You snarl and spit at the ork. You watch the hope fade from his eyes.
    
    -> killed_by_daniel

= killed_by_daniel

~ persist_sharptoof_dislikes_betrayal = true

Before you can react, Daniel kicks the boiling stew pot into you.

* [Dodge the stew]

- You stagger backwards, desparately trying to avoid the broth.

But the boiling stew splashes across yout legs, causing you to scream out in pain.

Daniel leaps upon you spitting and snarling, chef's cleaver in hand, and hacks away at your face and neck.

The pain fades swiftly.

-> death

= feed_da_boss

~ has_stew = true
~ trophy_found_stew ++
# TROPHY: found_stew

"Sure fing," Sharptoof says.

He spoons some goop from the cauldron into a large bowl.

"One squig surprise for da Warboss," he says, beaming. "Pipin' 'ot."

You take the food and walk away.

-> go_to_main

= feed_the_boss

~ has_stew = true
~ trophy_found_stew ++
# TROPHY: found_stew

"Urgh, that big burly brute," Daniel scoffs.

"Yes, I have some mushroom stew for him. Not that that philistine will appreciate it. In fact he'll probably try and kill you."

He spoons some goop from the cauldron into a large bowl.

Thanking Daniel, you take the food and walk away.

-> go_to_main


= investigation_sharptoof

"Krugg? Da mek?"

A shadow passes over Sharptoof's face - something you've never seen in an ork before. Fear.

"Nah, I don't know nuffink about it," he mumbles, stirring the pot and avoiding your gaze.

* [Fair enough]

- Suddenly he fixes you a glare.

"I ent scared. Nah. Nah he's just... weird, dat's all. Sorta... unnatural."

"I saw him doing fings to snotlings that weren't right," he says with a shiver.

~ obj_clue_sharptoof = true

-> sharptoof_menu

= investigation_daniel

~ obj_clue_sharptoof  = true

"You mean the local mek?" Daniel shakes his head sadly. A flash of horror passes his eyes before he replies again.

"There's something strange about that one. He's not like the other orks. And not in a nice way, like you and me."

Daniel falls silent a moment, lost in thought.

* "Daniel?"[] you prompt.
    Daniel stirs.
* Wait

- "Damaged. That's the word," he says slowly.

"I saw old Krugg getting up to some very unorthodox behaviour."

The old twinkle seems to return to Daniel's eye.

"Un-ork-odox, if you will, ha! Um, sorry."

* "What was he doing?"[] you ask.

- "Oh I wouldn't like to say. Unspeakable things with snotlings and pliers and electrodes and maybe a drill? Urgh, make me shiver just to think of it."

* "Isn't that normal orky stuff?"[] you ask.

- "Hardly. Orks aren't... cruel." Daniel muses.

"They don't have the patience, the creativity, to engage in properly sadistic tortures. Orks like the fight - good, hot-blooded fun between competing parties. Most orks actually prefer to be the underdog, scrapping against the odds. They find it heroic, or something. Imbecillic if you ask me."

"Oh look at me, jabbering on. What was the question?"

* "Never mind, you've been a great help."[] you say.

-> daniel_menu