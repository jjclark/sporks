// lingo stuff
// https://steamcommunity.com/sharedfiles/filedetails/?id=1122960642
// http://www.the-waaagh.com/a-to-z.html#s
// https://www.text-image.com/convert/pic2ascii.cgi

INCLUDE globals.ink
INCLUDE chompa.ink
INCLUDE wendy.ink
INCLUDE krugg.ink
INCLUDE daniel.ink
INCLUDE skullkrusha.ink

VAR persist_skip_intro = false

VAR obj_orks_beaten = 0

VAR obj_met_wendy = false
VAR obj_met_chompa = false
VAR obj_met_krugg = false
VAR obj_met_sharptoof = false
VAR met_daniel = false
VAR obj_met_skullkrusha = false

VAR obj_skullkrusha_gone = false
VAR obj_krugg_dead = false
VAR obj_wendy_dead = false
VAR obj_chompa_dead = false

VAR wendy_done = false
VAR chompa_done = false
VAR sharptoof_done = false
VAR krugg_done = false
VAR skullkrusha_done = false

VAR enable_krugg_chompa = false
VAR needs_armor = false
VAR needs_daniel = false
VAR needs_wendy = false

// -1 means a trophy hasn't been started
// 0 means started but not finished
// >0 means finished (can happen more than once)
VAR trophy_woo_krugg = -1
VAR trophy_woo_chompa = -1
VAR trophy_woo_wendy = -1
VAR trophy_woo_sharptoof = -1
VAR trophy_woo_skullkrusha = -1

VAR trophy_something_more = -1
VAR trophy_something_waargh = -1
VAR trophy_mission_accomplished = -1
VAR trophy_warp_waargh = -1
VAR trophy_friendzone = -1

VAR trophy_kill_krugg = -1
VAR trophy_bottoms_up = -1
VAR trophy_secrets = -1
VAR trophy_orkicide = -1
VAR trophy_way_of_wendy = -1
VAR trophy_death = -1
VAR trophy_introduce_wendy = -1
VAR trophy_tip = -1
VAR trophy_case_closed = -1

// deprecated
VAR trophy_liberator = -1
VAR trophy_sticks_and_stones = -1

VAR has_armour = false
VAR persist_used_armour = false
VAR trophy_found_armour = -1

VAR has_meat = false
VAR persist_used_meat = false
VAR trophy_found_meat = -1

VAR has_stew = false
VAR persist_used_stew = false
VAR trophy_found_stew = -1

VAR has_meat_stew = false
VAR persist_used_meat_stew = false
VAR trophy_found_meat_stew = -1

VAR has_artefact = false
VAR persist_used_artefact = false
VAR trophy_found_artefact = -1

VAR has_money = false
VAR persist_used_money = false
VAR trophy_found_money = -1

VAR has_secret = false
VAR persist_used_secret = false
VAR trophy_found_secret = -1

{ persist_skip_intro == false:
    -> main_intro
- else:
    You stand in the center of a raucous space Ork encampment.
    * [Look around]
        -> main

}
~ persist_skip_intro = true

== main_intro ==

In the grim darkness of the future, they say, there is only war.

You stand in the center of a raucous space ork encampment, and wonder if maybe you can find something more.

* [Look around]
    -> main

== function unlock_warchief() ==
~ return !obj_skullkrusha_gone && (obj_orks_beaten > 1 || has_armour || has_secret)

== function show_daniel() ==
~ return obj_orks_beaten > 0

== function dead_end() ==
~ return wendy_done && chompa_done && krugg_done && sharptoof_done && skullkrusha_done

== main ==

// In the main game, we'll return to our custom menu
// And never see any of this stuff
// We'll keep this stuff here for posterity though
# CLEAR
# MENU 

// Need a stub here or it won't reload
+ . =-> DONE


== death ==

# AUDIO: death
~ trophy_death++
# TROPHY: death

// Note that we depend on custom code for the restart,
// because we preserve profile variables

+ [YOU DIED]
    # DEATH
    -> DONE // the writer won't restart here, but the game will


== go_to_main ==

+ [Go back]
     -> main

== exit ==

You survey the camp and consider the friends you've made.

You are drinking buddies with Chompa. You've saved Krugg's soul. Wendy will tolerate you. No-one else knows Sharptoof's true name. Skullkrusha has accepted you as part of the 'krew'.

You sit by the fire with these unusual companions, and let yourself be soothed by the warmth.

+ [Let the good times roll]
    ~ trophy_friendzone++
    # TROPHY: friendzone
    # ENDING: friendzone.png In the grim darkness of the future, you have found a little light.
    -> fin_good

== profiles ==

// TODO I probably need to pull this out of the story and make it an overlay
// Maybe in the frontend we always show the ork's profile? We'll see
// For now, we'll just crowbar it in

Profiles:

+ [Chompa]
    -> Chompa.profile
+ [Wendy]
    -> Wendy.profile
+ [Krugg]
    -> Krugg.profile
+ { obj_met_sharptoof } [Sharptoof]
    -> Sharptoof.profile
+ { obj_met_skullkrusha } [Skullkrusha]
    -> Skullkrusha.profile
+ [<< BACK] -> main


== hints ==

GAME HINTS

There are five orks to 'date', two of them being unlocked by surviving other dates.

There are three victory states which will end the game. Death will reset everything apart from character unlocks and profiles.

Use the profile for hints about how to survive a date.

+ WALKTHROUGH (spoilers!)

    The best way to play the game, in the author's humble opinion, is this:

    \* Go to the bar and beat Chompa in a drinking contest (just keep drinking)
    \* Get the armour off the mechanic, Krugg (keep quiet and say you're there for the armour)
    \* You've now unlocked the Chef and Warchief.
    \* You can run away with the chef if you speak in fancy english (rather than orkish) and just roll with it.
    \*You can be the Warboss's right-hand-man if you flatter him a bit and join his Waargh
    \* Or expose yourself as an Imperium spybot on a mission to assassinate the warchief. You need to get him to give you his secret, so flatter and entertain him, OR bring him food and be scared of him.
    
    + + [<< BACK] -> main

+ [<< BACK] -> main


== fin_good

# AUDIO: ending-good

 -> END
    
== fin_bad

# AUDIO: ending-bad

 -> END

== fin
    
-> END