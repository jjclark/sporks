/*
  Chompa likes drinking. You 'beat' him at a drinking game.
  
  Chompa's story is really easy, deliberately so. I want a simple path so that you can easily get an unlock under your belt to unlock Daniel.
*/

// TODO all this needs review I think - it's not really working
VAR persist_chompa_likes_drinking = false
VAR persist_chompa_likes_drinking2 = false
VAR persist_chompa_likes_drinking3 = false
VAR persist_chompa_dislikes_sobriety = false
VAR persist_chompa_dislikes_fancy_stuff = false
// VAR persist_chompa_dislikes_inadequacy = false

VAR chompa_asleep = false
VAR chompa_drinks = 0
VAR chompa_info = 0
VAR chompa_rejected = false
VAR times_quit = 0
VAR gretchen_intro = false

=== function status_chompa ===
{  
- obj_chompa_dead:
    ~ return "Chompa's bar stands even quieter than usual"
- chompa_done:
    ~ return "Chompa stands behind his bar"
- chompa_asleep: 
        ~ return "Chompa is moving heavily around the bar"
- obj_met_chompa:
    ~ return "Chompa wipes his bar top listlessly, nursing a tankard"
- else:
    ~ return "An ork wipes a bar top listlessly, nursing a tankard"
}

=== Chompa

= profile

CHOMPA

Likes:
    \* { -persist_chompa_likes_drinking:
        <> Drinkin'
        -else: <> ???
        }
    \* { -persist_chompa_likes_drinking2:
    <> Drinkin'
    -else: <> ???
    }
    \* { -persist_chompa_likes_drinking3:
        <> DRINKING'
        -else: <> ???
        }

Dislikes:
\* { -persist_chompa_dislikes_sobriety:
        <> Hangovers
        -else: <> ???
        }
\* { -persist_chompa_dislikes_fancy_stuff:
        <> Fancy stuff
        -else: <> ???
        }
// \* { -persist_chompa_dislikes_inadequacy:
//         <> Feelings of inadequacy
//         -else: <> ???
//         }


+ [<< BACK] -> main

= start

{
    - obj_chompa_dead:
        Chompa's bar is a smouldering ruin.

        There is nothing to see here.
        -> go_to_main
    - chompa_asleep:
        { stopping:
            -You find Chompa at the bar. He's moving slowly and his eyes are redder than usual.
            ~ chompa_asleep = false

            "Bloody rokkit fuel," he mumbles to himself.

            "Oh, ello," he says, noticing you.

            He pulls his hat from his head and squeezes it uncomfortably.
            
            * "My head hurts[."]," you admit. # GUIDE woo_chompa/start
                    -> chompa_mates
            * [Leave the bar] You leave the hungover ork alone.
                    -> main

            -You join Chompa for a drink at the bar.

            {~You happily talk nonsense over a grog|You complain companionably about the warboss|You listen as the gretchen plays the greens}.

            As the conversation grows thin and your head starts to feel light, you thank Chompa and leave the bar.

            + [Visit the gretchen] You leave the bar.
                -> gretchen
            + [Head back] You leave the bar.
                -> main
        }
    - chompa_rejected:
        Chompa notices you approach and skulks down the bar, suddenly looking busy and avoiding your gaze.

        + [Head back] You leave him alone.
            -> main
    
    - obj_met_chompa:
        -> root_menu
    
    -else: 
        -> chompa_intro
}

= chompa_intro
An ork stands behind a calamity of bar made of corrugated steel sheets, open to the sky. Tall, wonky-looking stools stand before it, while a scattering of empty tables sulk on the hard clay.

The bar is empty, save for a gretchen playing a battered old upright piano.

A series of huge metal tanks loom over the bar, a lattice framework at their base lined with bottles and tankards.

The ork slurps from a metal tankard, burping and wiping his mouth with the back of his hand. He wears a scruff of a fur hat and a worn, burgundy leather jerkin.

* [Approach the bar]
    -> root_menu
* [Approach the gretchen]
     -> gretchen

= menu_loop

+ [Ask another question]
    -> root_menu
+ [Head back]
    -> main

= root_menu

{ obj_met_chompa == false:
    ~ obj_met_chompa = true
    ~ persist_chompa_likes_drinking = true

    "Welcome ta Chompaz Bar!" the ork beams.
    
    His grin fades as he looks down the empty bar.
    
    "Ent much goin' on, I's afraid."

-else: 
        Chompa takes a slurp of grog.
}

+ [Approach the gretchen]
     -> gretchen

* { gretchen_intro } ["What's with the gretchen?"] "What's with him?" you ask, nodding towards the gretchen.
    -> da_greens

* "Why'z it so quiet?"[] you ask.
    -> why_so_quiet

* { root_menu > 3 } "Did your hat just squeak?"[] you ask.
    Chompa whips his arm and smashes his hat/cloth onto the bar with a violent thump.
    
    "Nope," he grunts.
    * * [Fair enough]
        -> root_menu

+ "How about some grog?"[] you ask. # GUIDE secrets/start bottoms_up/start
    -> start_drinking(false)
    { "Good choice, cos' dat's all we got!"|"Tuffened up, have ya?"}

+ "I don't suppose you can rustle up a pina colada?"[] you ask cheerfully. 
    ~chompa_rejected = true
    
    The ork scowls at you.
    
    "Anovver fancy git, huh? Genderification, is what it is," he mumbles.
    
    "Dis a ork bar," Chompa says, leaning menacingly over the counter. "Not a fancy 'saloooon'. We serve grog and talk rubbish. Sometimes we serve rubbish talk grog. Dats it."

    + + "Do you mean 'gentrification'?" [] you ask.
    + + "I'm sorry, I meant no offence." [] you say.
    
    - - "Shove off you bloody weirdo or I'll gut ya."
    
    Chompa turns his back to you, pulls off his furry hat, and starts wiping down mugs with it. You won't get any more conversation out of this ork.

    + + + [Head back] You leave him to it.
        -> main

* { root_menu > 0 } { obj_krugg_investigation_started } "Do you know anything about Krugg's snotlings?"  # GUIDE case_closed
    "Krugg?" Chompa spits.
    
    "Ya know, Chompa's an easy goin' fella. I like a good ork. Even like a bad one. But dat mek?"

    Chompa shakes his head.

    "He changed after a scavenger run. Went all weird, started upsetting snotlings, stopped drinkin' at me bar."

    * * "Thanks."

    "Watch yerself around dat one," Chompa advises sagely.

    ~ obj_clue_chompa = true

    -> root_menu
        
+ [Leave the bar]
    You leave Chompa to tend his lonesome bar.
    -> go_to_main

= why_so_quiet

"Orks like two fings, right," Chompa scowls, holding up three thick fingers.
 "Dey like fightin', and dey like drinkin'. Dis place should be packed like squigs in a krate."

Chompa looks at his hand and shrugs.

"But da boss got everyone <i>workin</i>'  instead", he spits. "Boyz ent got time for anyfink else."

-> why_so_quiet_menu

= why_so_quiet_menu

* "What is everyone working on?"
    "Boss is setting up a Waargh," Chompa grunts. "Got da lads runnin' round, buildin' stuff, findin' more boyz, gettin mor turf."

    "He ent even arsked me fer a special grog," Chompa finishes sadly, staring down at his bar.

    "I was gonna name it Graargh."
    -> why_so_quiet_menu
* "Tell me about the boss."
    Chompa looks over his shoulder before leaning in conspiritorially. A powerful waft of stale grog creeps over you.
    
    "Right nasty git if you arsk me," he growls. "Always shoutin' at the da lads, pushin' 'is weight around. Never comes to Chompa's bar. Always kicking up a fuss. Never known such a ugly ork."

    Chompa pulls his hat off his head and wipes a puddle off the bar.
    -> why_so_quiet_menu
* "You got any time for love, Chompa?"
    "Lav? Wot's dat?" he asks, head cocked to one side.

    * * "It's that tingly feeling deep inside you[."],"
    * * "It's when you a suffused with an internal warmth that makes the bad stuff go away[."],"
    * * "It's what we fight for[."],"

    - - <> you say.
    
    "Oh," Chompa nods, wisely. "Ya means <i>grog</i>. Chompa always got time fer grog."

    "Speakin of!" he grins, raising his tankard. He drainings it and pours himself a refill.
    -> why_so_quiet_menu
+ That's all for now.
    -> root_menu

= gretchen_debug

~has_money = true

-> gretchen

= gretchen

{ -!gretchen_intro:
    ~gretchen_intro = true
    The piano stands off to one side of the bar. It has seen better days - you are amazed it can hold a tune. A tin cup sits on top of it.

    A gretchen sits at the stool, his little legs hanging above the floor. He wears a pair of black sunglasses and a grey waistcost, his sleeves rolled up his bony green arms.
    
    With his green, hooked nose held high, he fingers the keys with a languid grace.
- else:
    The gretchen plays the blues.
}

+ [Speak to the gretchen] 
    <>{ stopping:
        - "Ello", you say, but the gretchen ignores you.
        - { shuffle:
            - "Hey man," you say. The gretchen doesn't respond.
            - The gretchen doesn't appear to notice you.
            - The gretchen doesn't seem to hear you.
        }
    }

    -> gretchen
* { has_money} [Tip the gretchen] # REQUIRE money # GUIDE tip/end
    # USE_ITEM: money
    ~ trophy_tip++
    ~ has_money = false
    # TROPHY: tip
    You drop a few teeth into the jar on top of the piano.

    "Fanks, pal," the gretchen purrs.
    -> gretchen
+ [Go back to the bar]
    -> root_menu

= da_greens
{ stopping:
    -"Oh, e's been 'ere ages." Chompa shrugs.

    "'E just turned up one day, playin' da greens. I kinda like havin him around, ya know? Gives dis place a little... ambulance."
    - Chompa gazes at the gretchen and taps a finger out of time.
}

* * "The... greens?"
    "Yer. You know, 'cause it captures the deepest, darkest greens in an ork's 'art."

    Chompa rubs his eyes.
    -> da_greens
* * "You mean ambiance?"
    "Dat's da fingy," Chompa nods. "Am-bew-larnce."

    -> da_greens
* * "Fair enough[."], you say.
    -> root_menu

= start_drinking(interrupt)
    { 
        -interrupt:
        -start_drinking == 1:
            Chompa fills a metal tankard and slams it hard on the table. Froth spills out over the top and fizzes on the bar.

            "Lurvely stuff, dis," he grins. "Made wiv real rokkit fuel."

            The air above the grog seems to shimmer. The stench is vile.

            Chompa leans in conspiritorially.

            "Nabbed it off dat twitchy git, Krugg."

            ~ enable_krugg_chompa = true
 
        -else:
            "{Tuffened up, have ya?|Ready fer more?|Time for anovver one, eh?}"

            Chompa pours another tankard of grog and slides it across the bar.
    }

    * "Doesn't Krugg need that fuel?"[] you ask.
        "Nah, e's gone crazy lately," Chompa grunts. "'E won't miss it."

        -> start_drinking(true)
    * "What you charging?"[] you ask.
        "Nuffink!" Chompa cheers.
        
        "I, er, could use da company," he adds, sadly.

        * * "Well then, let's drink!"[] you say.
            "Dat's wot I've been tryin to say!" Chompa growls.

            -> start_drinking(true)
        * * "That's not very economical[."]," you say. "How can you afford to just give grog away?"

            "I ent got time to worry about the economy," Chompa growls. "I got a bizniss ta run."
            -> start_drinking(true)
    + [Drink] You take a deep draught. # GUIDE secrets/a bottoms_up/drink
        ~ chompa_drinks = 0
        ~ chompa_info = 0
        You are instantly overwhelmed with a sense of drowning and despair. You feel it burn down your throat and fizz through your stomach. You feel your vision dim and your toes tingle. You feel your bile rise and your hairs stand on end.
        
        It's not bad.

        + + [Take another sip] You drink a little more.
        
            ->  how_about_a_game
    
    + [Maybe not] You push the tankard away. Today is not a good day to die.
         {  times_quit == 2:
            ~chompa_rejected = true
            "Again?" Chompa spits angrily.
            
            "Dat's it - yer barred. Geddout!"

            + + [Walk away]
                -> main
        -else: 
            ~ times_quit ++
            "Woss wrong wiv me grog?" Chompa scoffs in disgust.
            
            "Bah. Waste not, eh?" He grabs your tankard and downs the grog noisily.

            -> menu_loop
        }
   
= how_about_a_game

{ stopping:
    -"So you can handle yer grog, eh?" Chompa says, approvingly.

        "'Owz about a little game, den?"

        He slides another frothing tankard over to you, his eyes twinkling.
    - "So, you in?" Chompa asks.
}

* "What sort of game?"
    "I calls it da Drinking Game", Chompa says proudly. "Me own invenshun."

    "It's very complicated," he adds.

    * * "What are the rules?"[] you ask.

        "Well," Chompa says, drawing in a deep breath and counting on his thick fingers. "First: every time I drink, you drink."

        He pauses as deep furrows creasing his forehead.

        * * * "... Go on[."]," you prompt.

        - - - "Er, second: when you drink, I drink."

        Chompa stands still a moment, then breathes out slowly.

        "You got all that?" he asks, looking worried.

        * * * "Sounds like a recipe for a hangover[."]," you say.
            "Wot, dis stuff?" Chompa asks, raising his tankard.

            "Nah, dis is da best 'angover cure I ever 'ad. Coupla these first thing in da afternoon and you'll be right as rocks," he beams.

            -> how_about_a_game

        * * *"Got it[."]," you say. -> how_about_a_game

        * * * "When do we stop?"[] you ask.

            "Stop?" Chompa asks, looking confused.

            * * * * "Ok, I  get the idea[."]," you say -> how_about_a_game
    * * "I'll work it out[."]," you say. -> how_about_a_game
+ "Yer on!"[] you say, banging the bar with a fist. # GUIDE secrets/b bottoms_up/down-grog-1
    "Addaboy!" Chompa cheers.
    
    You pick up the grog and drink it down, feeling it burn away your stomach lining.
    
    Chompa raises his own tankard and matches your pace.
+ [Down the grog] <> You pick it up and sink the lot one long chug. # GUIDE secrets/c bottoms_up/down-grog-2
    Chompa laughs and bangs on the bar excitedly, then raises his own tankard to his mouth, grog streaming everywhere as he slurps and gulps it down.
+ "Nah. I've got fings ta do."
    "Suit yerself."
    
    Chompa picks up your tankard and chugs it down, then skulks off down the bar, muttering to himself.
    
    -> menu_loop

- "Ah! Dat's da stuff!"

Chompa wipes his mouth and slams the tankard onto the bar with a crash.

-> drink_up(true, false)

=drink_up_debug

-> drink_up(false, true)

= drink_up(skipIntro, allowChat)

{ chompa_drinks > 4:
    -> chompa_out_drunk
}

{ skipIntro == false: 
    { cycle:
        -Chompa pours out a couple more grogs.
        -Chompa staggers over to the still and pours out more grog.
        -Chompa puts two more bubbling tankards down and leans heavily over the bar.
        -Two Chompas slam a couple more tankards onto the bar.
        
        You brain feels hazy. You feel your sense of self eroding.
        -Both Chompas sway from side to side, drooling. You pour out the next round yourself.
    }<>
}

* { allowChat && chompa_info ==  0} "Cheers!"[] you say. # GUIDE secrets/1
    -> drinking_digression0
* { allowChat && chompa_info ==  1} "So you were in the seige of Fort Mork?" [] you ask. # GUIDE secrets/2
    -> drinking_digression1
* { allowChat && chompa_info ==  2}  "Did you say you were an inventor?" [] you ask. # GUIDE secrets/3
    -> drinking_digression2
* { allowChat && chompa_info ==  3}  "So Skullkrusha likes da troof serum, huh?" # GUIDE secrets/4
    -> drinking_digression3
+ { chompa_drinks ==  4} [Drink] You drink. Chompa drinks. # GUIDE bottoms_up/end 
    ~ chompa_drinks += 1
    -> drink_up(false, true)
+ { chompa_drinks < 4} [Drink] You drink. Chompa drinks. # GUIDE bottoms_up/drink-more
    ~ chompa_drinks += 1
    -> drink_up(false, true)
+ { chompa_info ==  4} [Retire] # GUIDE secrets/retire
    -> quit_while_ahead
+ { chompa_info !=  4} [Retire]
    -> bugger_off

= quit_while_ahead

You shake your head. You brain sloshes around.

You stop shaking your head.

"Ha! Ha ha ha HA!" Chompa laughs obnoxiously.

"HA!" He spurts, then blurts out a strange sort of hiccup and collapses behind the bar.

You try to concentrate. You are sure you have learned something important.

* Chompa's kind of an idiot[], you think.

    But that doesn't seem very helpful to you right now.
* [Skullkrusha hates himself] That's it! The Ork Warboss, Skullkrusha, secretly hates himself. # GUIDE secrets/end # ACQUIRE secret
    ~ has_secret = true
    # TROPHY: secrets
    ~ trophy_secrets = true
    Maybe you can use this information to your advantage.
* You actually really like grog[].

    Yep, that's it.

-You clutch your remaining wit about you and stagger off to find somewhere to sleep.
    -> go_to_main

= chompa_out_drunk

Chompa stares you in the eye, wobbling. He raises a finger and wags it.

"Buggrit," he announces.

He belches loudly in your face, then collapses onto the bar.

He appears to be fast asleep.

~ obj_orks_beaten += 1
~persist_chompa_likes_drinking3 = true
~ trophy_bottoms_up++
# TROPHY: bottoms_up    

+ [Leave the bar] You leave the bar.
    Well, you try.

    As you step away, your legs do something terribly complicated beneath you. You suddenly cannot remember what knees are for. And why are ankles?
    
     * * You fall heavily to the floor.

     * * * You remember very little of what happens next.

    * * * * This is probably for the best.

    * * * * * Let's just say it got rough.

    // TODO - fake death sequence
    When you wake up, you have many regrets. You are also burdened with the feeling you've forgotten something important.

    ~ chompa_asleep = true
    -> go_to_main
    
= drinking_digression0

~chompa_info++
"Aaaah!" sighs Chompa, looking at his grog longingly. "Last time I had rokkit grog like dis was Bozonga's great Waargh."

* "You were in a Waargh?"[] you ask.

- "Yer. Never seen such a big 'orde. Not even in da seige of Fort Mork."

"We woz a sea of greenskinz, lootin' and stompin' and killin' all over da sector. Dey called us a 'foul-smelling blight upon the civilised galaxy'."

Chompa chuckles and strokes his chin.

"Good timez."

* "That sounds incredible!"[] you say.

- "Yer. Busiest bar I ever worked. Some of da best grog, too."

* "Wait, you mean you just tended bar?"[] you ask.

- Chompa shrugs.

"Someone had to."

-> drink_up(true, false)

= drinking_digression1

~chompa_info++
"Dat's what I sed," Chompa nods.

"Dat was tuff - I fort we were gonners."

* "What happened?"[] you ask.

- "Oh you know. Couple ov humies tryin' ta take down the fort. Mostly it were a bit noisy, which made it hard to work on me invenshuns."

"Fings got rough as da weeks dragged on though. Almost ran outta grog - dat's when I found da rokkit fuel."

* "So you just tended bar there, too."

- "No, I brewed da grog, too!" Chompa says, chagrined. "Important job, keepin da bars open. Gotta take da edge off some'ow."

"Anway, you seen da size of da humies' shootas? Can't go fightin' dem wivvout some booze inside ya. Ain't right."

-> drink_up(true, false)

= drinking_digression2

~chompa_info++
"Sure, I 'vented loadsa stuff. Who knows, maybe greenskinz would all be ded if it weren't fer me."

"Tell you da troof," Chompa says, leaning in and tapping his nose. "I'm a bit of a legend. 'Av to keep ma real name secret."

* "What's your real name, then?"

-Chompa pulls up defensively.

"Ent tellin'", he says.

* "Can you tell me what you've invented?"

-"Well let me see," Chompa held up a hand and started counting off his fingers.

"Dere's squig grog, rokkit grog, bug grog, grog grog - dat's a good one - winter grog, cold grog, troof serum - da boss liked dat one."

It looks like he might go on for a while.

- * "Hang on, you mean you've just invented different types of grog?"

-"Yer welcome. Anuvver?"

-> drink_up(true, false)

= drinking_digression3

~chompa_info++
"Lovesh it - but won't touch it again. You wouldn't belief da fings 'e said."

Chompa looks over his shoulder leans forward conspiratorially, pulling his hat over his face.

"Shed 'e wash sorry fer bein' a, er, wot wash it? Bastid, dats it. Sez 'e was lonely. Sez-" Chompa burps loudly, for effect, then continues. "Sez 'e 'ates himshelf."

~ persist_skullkrusha_dislikes_himself = true

Chompa stares sadly into his grog.

* "But thatsh crazy!"[] you slur.

- "Nah, makesh all too much shense," Chompa says, taking a deep draught of grog.

"Orksh is messhed up. Da only fing we duz, da only fing that dat makesh shense, ish... fightin'!"

* "But why?"

- "'Caush if we ish angry at everyone else, we don't avta be angry at ourshelves." Chompa sighs.

He goes quiet for a minute, staring into the middle distance. You wonder if you should say something - when he starts speaking again.

"Deep down, orksh hate demshelves firsht, and everyfin elsh shecond. Anger is da only way we can express ourselvesh."

You feel like this is important.

"And da bossh," Choma says, cheering up. "Da bossh ish an artist."

* "Spand flotscabowt woo pffp?" [] you slur.

- "Wot?" Chompa asks.

* "And what about you?" [] you repeat, carefully.

- "I ent angry!" Chompa says, raising his arms high and knocking over an empty tankard.

"I's drunk!" he barks happily.

* [Nod and drink up.] You nod and reach for your tankard, but it swims unnaturally across the table.

You pounce onto it, grabbing it between both hands, and tip it carefully into your mouth.

You slurp and feel a layer of meaning peel away from your world. A tiny voice tells you to quit while you can.

-  -> drink_up(true, false)


= bugger_off
~ times_quit ++

You decide that this game isn't for you.

You pour your tankard onto the floor. Chompa cheers victoriously.

* [Walk it off]

- You walk away from the bar while Chompa pours himself a celebratory grog.

You try and walk off your spinning head.

* [Back to camp]
    -> main

= krugg

{
    -chompa_asleep:
        Chompa lies sleeping on the bar.

        Krugg gives the bar a frustated kick, then walks away.

        He halts, turns, and pours himself a tankard of grog.

        He spits on Chompa and walks away.

        * [Back to camp]
            -> main

    -else:
        Krugg approaches the bar, his ammunition chain rattling, and plants his feet squarely on the ground.

        "Oi! Chompa!" he shouts.

        Chompa looks up from the tankard he's wiping.

        "Dis is for stealin' my juice!" Krugg roars.

        * [Cover your ears] You cover your ears in anticipation. # GUIDE orkicide/a
            -> krugg2

}

= chompa_mates

"Listen, dat's never happened to be before." Chompa looks embarrassed. "But I gotta admit - I'm impressed. Never met anyone else who can drink like dat."

"We should do it again sometime," he says, staring into his hands.

* "I'd like that[.]," you say. # GUIDE woo_chompa/end

~ chompa_done = true
~ trophy_woo_chompa++
# TROPHY: woo_chompa 

- Chompa grunts and shoves his hat back on.

"I gotta go find some grub or sommink," he grumbles, then turns and walks away.

+ [Visit the gretchen] You leave the bar.
    -> gretchen
+ [Head back] You leave the bar.
    -> main

= krugg2

Krugg opens fire.

His gun roars into a fury of thunder and fire. Bullets rattle through the bar as Krugg laughs maniacally. You see Chompa fall to the ground.

Suddenly, a line of bullets hammer into the huge grog still - and it explodes.

* [Take cover] You dive to the ground as the horizon erupts in fire.

- Heat and sound wash over you. You feel a weight press down on top of you.

* [Shift the weight] You shrug the weight off you. Krugg's charred, lifeless body rolls along the ground. # GUIDE orkicide/end

- You look up to see a mushroom cloud billowing into life where Chompa's bar used to stand.

A confusion of orks runs around, shouting insults and ignoring orders.

# TROPHY: orkicide
~ trophy_orkicide++
~ obj_chompa_dead=true
~ obj_krugg_dead=true

* [Time to leave] You take your leave.
    -> main
