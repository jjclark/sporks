/*
    Krugg is the mechanic. He has a little snotling mate called Boris.

    TODO this is all out of date now really

    He protects some armour (which you can give to Skullkrusha or use to survive insulting wendy). You can have the armour if you take the chaos artefact or kill him.

    He is under the influence of a Chaos artefact strapped into a bomb. You can take the artefact and give it to Skullkrusha to trigger a chaos ending.

    You can kill Krugg by defusing the bomb (be sure to step back). When krugg is dead, Boris can be freed.

    If you've spoken to Chompa, you can tell Krugg that Chompa stole his fuel. This kills both orks.
*/
`
VAR did_step_back = false
VAR boris_chained = true

VAR mention_armour = false
VAR mention_slackjaw = false
VAR enable_curtain_discussion = false

// Clues and investigation persist after death because you the player retain the info
// Also it's a MASSIVE pain to die and have to redo it
VAR obj_krugg_investigation_started = false
VAR obj_krugg_investigation_score = 0

VAR obj_clue_chompa = false
VAR obj_clue_sharptoof = false
VAR obj_clue_wendy = false
VAR obj_clue_skullkrusha = false
VAR obj_clue_boris = false

VAR persist_krugg_likes_bombs = false
VAR persist_krugg_dislikes_surprises = false
VAR persist_krugg_dislikes_grots = false
VAR persist_krugg_dislikes_manners = false
VAR persist_krugg_dislikes_whispers = false

== function status_krugg ==
{
    -obj_krugg_dead:
        ~ return "Krugg's workshop stands empty and silent"
    -obj_met_krugg:
        ~ return "Krugg crashes around in his workshop"
    -else:
        ~return "Shrill screams echo out of a workshop"
}

== Krugg ==

= profile

KRUGG

Likes:
    \* { -persist_krugg_likes_bombs:
        <> Bombs
        -else: <> ???
        }

Dislikes:

    \* { -persist_krugg_dislikes_surprises:
        <> Surprises
        -else: <> ???
        }

    \* { -persist_krugg_dislikes_grots:
        <> Grots
        -else: <> ???
        }

    \* { -persist_krugg_dislikes_whispers:
        <> The terrible whispers
        -else: <> ???
        }

+ [ << BACK] -> main

= start

{
-obj_krugg_dead:
    -> workshop_empty
-obj_met_krugg:
    You enter the workshop.
    -> main_branch
-else:
    -> intro
}

= intro

You see a boxy metal workshop, the doorway adorned with metal skulls. A huge pile of junk leans against one side, spilling out onto the red clay.

An angry commotion of shouting, metallic banging, and the occasional high-pitched scream rages from inside.

// No guides at this pre-root menu. No point.
+ [Go inside]
+ [Maybe come back later] You'll come back later.
    -> main

- You peer around the doorway.

You see a filthy workshop - a horror of scrap metal, exposed wiring and leaking barrels. A rusting Killa Kan sits in the far corner, red light blinking slowly.

An ork clad in grubby overalls waves a thick wrench around, clanging it into workbenches and shelves and boxes.

A snotling sits underneath one of the workbenches, skinny hands over its huge ears, and trembles with fear.

+ "Whuzaaaaaap?!"[] you shout. # GUIDE death/krugg_surprise1
    -> death_by_surprise
+ [Wait]

- "First it's Ted!" the ork booms, thwacking his wrench on the workbench with a CLANG and a terrified yelp from the snotling.

"Den Stinky!" CLANG.

"Now Digga!" CLANG.

"Shaddap, you lot!" The ork yells over his shoulder. "Krugg don't wanna hear it!"

You look around - there is no-one else in the room.

+ "Excuse me[?"]," you say. # GUIDE death/krugg_surprise2
    -> death_by_surprise
+ [Wait]

- The ork called Krugg squats before the workbench, places his wrench underneath the snotling's chin, and raises its snivelling eyes level with his own.

"Where's yer mates at, Boris?" the ork growls. "Is Dok Slackjaw up tah somefink?"

The snotling sniffs and shakes his head, huge eyes trembling.

"Stoopid grot," Krugg grumbles, then cocks his ears as if listening to something.

~ persist_krugg_dislikes_grots = true

Suddenly he stands, shakes his head and smacks his forehead with his palm.

"Shaddap shaddap shaddap!" he rages.

+ "Oi, Krugg[."]," you say.
+ [Rap on the wall] You rap a kunckle against the wall of the workshop.

- ~ obj_met_krugg = true
~ persist_krugg_dislikes_surprises = true

Krugg, startled, whips around to face you.

"Oi! 'Ow long you bin standing dere?" he demands. "Krugg hates surprises." # TODO I hate this line now

+ "Sorry[."]," you say.
+ [Snort] You snort gruffly.
+ [Walk away] You walk away without a word.
    "Yer," Krugg shouts after you. "You betta walk away!"
    
    You don't look back.

    + + [Menu]
        -> main
    
- Krugg stalks over to his workbench and drops the wrench loudly against it.
//- Krugg stalks over to his workbench, drops the wrench loudly against it and picks up a grenade, turning it over in his hands thoughtfully.
-> main_branch

= main_branch_debug 
~has_artefact = true
-> main_branch

= main_branch

// TODO create different branches for love and death so that we can simplify

{
-has_artefact: 
    {Krugg happily clatters around, whacking things with other things.|Krugg whistles cheerfully to himself.|Krugg potters around the workshop with an air of contentedness and the occasional flurry of violence.}

    { once: 
        -He seems a lot happier without the artefact.
    }
}
"What can I do ya for?" he asks.

// Not sure if this guide is right. But it's a useful clue I guess?
+ { mention_armour } { !has_armour } "Can I have dat armour?"[] you ask. # REQUIRE artefact # GUIDE found_armour/start
    { -has_artefact:
        -> gather_armor_happy
    -else:
        -> gather_armor_grumpy
    }  

* "Something up with your snotlings?"[] you ask. # GUIDE case_closed/start
    -> start_investigation

* "Wot ya doin in 'ere?"[] you ask.
    -> explain_workshop

 + { obj_krugg_investigation_started } "About your snotlings..." # GUIDE case_closed
    -> accuse

 + { obj_krugg_investigation_started } { !obj_clue_boris } [Talk to Boris] # GUIDE case_closed
    -> chat_to_boris

* { mention_slackjaw } "Tell me about Dok Slackjaw[."]," you say.
    -> discuss_slackjaw

* { !has_artefact } { enable_curtain_discussion } { !reveal_shrine } "Krugg, what's behind that curtain?" [] you ask.  # GUIDE kill_krugg/start
    -> reveal_shrine

+ { !has_artefact } { enable_curtain_discussion } { reveal_shrine } "Can we look at your thingy again?" [] you ask.  # GUIDE kill_krugg
    -> reveal_shrine

* { has_artefact } "Feeling a bit better, Krugg?" # GUIDE woo_krugg/end
    ~ obj_orks_beaten += 1
    ~ krugg_done = true
    ~ trophy_woo_krugg++
    # TROPHY: woo_krugg
    "Yer!" Krugg says, folding his arms and scratching his chin.
    
    "Krugg don't know what it is, but Krugg's not felt dis good in ages. Must be da company, eh?"

    He smiles in a horrifying manner.
    -> main_branch

* { !has_artefact }  { confession_menu } "You should give me the thingy." # GUIDE found_artefact/start
    -> give_the_thingy

+ { krugg_done } Hang out.[] You hang out with Krugg for a while.
        He cheerfully shows you some of his latest creations - various explosives, automated weapon systems and traps.

    When you finally leave the workshop, you can't help but feel lucky to still be alive.

    -> go_to_main

* { enable_krugg_chompa } "Chompa stole yer fuel."[], you say. # GUIDE orkicide/start
    -> chompa

+ [Leave]
    "Nuffink," you say and walk out of the workshop.
    
    + + [Go back]
        -> main

= death_by_manners
~ persist_krugg_dislikes_manners = true

"Please?!" he bellows. "Gerroutta Krugg's shop ya poncy git!"
    
Krugg picks up an axe from his workbench and hefts it menacingly.

* "Oh my I am so sorry!"
* "Umm, how about no?"
* "Nah."

- Krugg hurls the axe at you. You hear a sickening squelch and look down to see the heavy axe sticking out of your chest.

The last thing you notice is the sound of the snotling laughing.

-> death

= death_by_surprise

~ persist_krugg_dislikes_surprises = true
"AAARGH!!"

The ork spins around with a shout, grabbing a bolter pistol from the workbench, and pointing it at you. You hear the sound of the gun going off before you feel sharp spikes of pain lash across your body.

"Don't be sneaking up on Krugg!" he roars.

The world twists and turns impossibly as a flood of pain consumes you.

+ ["Ow!"]

-You try to scream, but everything goes dark.

-> death
    
= gather_armor_grumpy

"Wot? No way. Push off ya lanky git!" Krugg bellows.

+ "Um, please?" # GUIDE death/krugg_manners2
    -> death_by_manners
+ "Fine[."]," you say.
    -> main_branch

= gather_armor_happy

# USE_ITEM: artefact

"Da armah?" Krugg looks suspiciously at the armour, then back at you.

"Ya know what, as it's bin so quiet lately, old Krugg's in a good mood. Go on pal, ya look like ya could use it."

* [Accept the armour] # ACQUIRE armour # GUIDE found_armour/end

- ~has_armour=true
# TROPHY: found_armour
~trophy_found_armour++

You gratefully accept the armour.

-> main_branch

= explain_workshop

// TODO maybe mention the snotlings somewhere in this tree

~ mention_armour = true

"Buildin' stuff for da Warboss," Krugg says, gesturing across the piles of junk. "'E's got big plans."

"Krugg just fixed up some nice tuff armah. Anyone wearing that will be like a walkin' trukk."

Krugg gestures to the back of the workshop, where a chunky breastplate hangs from a headless, skeletal mannequin. It looks like fine gear, fit for a warboss.

"Lotta pressure on old Krugg," he frowns, tapping his thick skull. "Not many boyz round 'ere wot knows 'ow to fix stuff like dat."

* "Tell me about the warboss."[] you say.
    // TODO what if artefact is gone?
    Krugg's eyes glaze over and he stares at you blankly.

    "He's da one. He's da conduit. Wiv him, we can be free."

    He shivers and shakes his head, his eyes refocusing.

    "'E's bigger'n'me - bigger 'n anyone - and he says we orks got a dest'ney. Or summink. Krugg ent gettin on da wrong side of his choppa."
    -> explain_workshop_menu
* "It's a good job you're here."

- -> explain_workshop_next

= explain_workshop_menu
* "It's a good job you're here to fix this stuff."
- -> explain_workshop_next

= explain_workshop_next
~ mention_slackjaw = true
An angry shadow passes across Krugg's brow.

"Well, dere's Dok Slackjaw too, stealing Krugg's stuff, showin' off ta da boss."

Krugg twiches and looks over his shoulder, muttering something you can't hear.

Then he looks back at you, then frowns sheepishly.

* "You OK?"
* "Wot?"

-<> you say.

Krugg is silent a moment, then he carries on as if nothing happened.

"Fing is, mostly Krugg just hits fings wiv a spanna till dey start beepin'. Or 'splodin'. Wivvout da liddle lads around, Krugg gets a bit... lost."

Krugg fidgets with his wrench.

"Krugg always wanted to bang da war drums, ya know."

* "You don't say?"
* "Ha! Krugg da drummer boy!" [] you scoff.

- "Dey sed no," Krugg says, sadly.

"Sed Krugg had da riddem of a squig wiv da squits," he sniffs.

* [Best to leave it there] You decide to let that one lie.
    -> main_branch

= discuss_slackjaw

"Dok's a mek from the Green Bastids clan," Krugg says, speaking slowly and crunching his knuckles.

"Da warboss brought him in a while back. He reckons he's so tuff wiv his mekky arms and his krafty eyes. Erugh."

Krugg shudders.

"At least Krugg still got all 'is bits."

~ enable_curtain_discussion = true

"An' Krugg got something he ent got." Krugg mutters conspiratorially.

-> main_branch

= bomb_explain

- "Krugg forgets 'ow it works," Krugg says, reaching up and carefully picking up the artefact from the step ladder.

He roughly yanks a panel cover off - apparently the bolts weren't doing very much - exposing a jumble of cables and some vials of luminous liquid. 

The sound of whispering intensifies.

"Don't want it to go off. Reckon we should cut one of dese?" Krugg says, holding up the wires.

-> bomb_branch

= bomb_branch

+ "{~Try|How about|Maybe|I reckon|Must be|Obviously it's|Clearly|It's} the red one." # GUIDE kill_krugg/bomb death/krugg_bomb1
+ "{~Try|How about|Maybe|I reckon|Must be|Obviously it's|Clearly|It's} the blue one." # GUIDE kill_krugg/bomb death/krugg_bomb1
* [Step back] # GUIDE kill_krugg/safe
    ~did_step_back = true
    You take a slow step backwards, not taking your eyes off the box.

    -> bomb_branch
* "I don't think this is a good idea[."]," you say.

    Krugg shrugs.

    "Fine," he says, and puts the box back on the ladder.
    -> describe_shrine_menu

- "You sure?" Krugg grunts, peering into the box.

+ "Of course I'm sure!" # GUIDE kill_krugg/sure death/krugg_bomb1
+ "Um, let me take another look"
    -> bomb_branch
    
- Krugg snips the wire with a sniff.

"Dere," he says.
You wait a second, and nothing happens. You release a deep breath just as the bomb goes off.

With an eerie silence and a rush of wind, the air around Krugg seems to fold inside itself, blurring and twisting horribly. You hear a terrible screaming from somewhere - maybe it's you? - and most of Krugg disappears.

{ did_step_back: -> survive_bomb }

+ [Listen to the call]

You feel something pulling at your body, at your mind, at your very soul. You feel an overwhelming sense of love and pain and loss and fear all at once. You feel the elation of coming home after a long cold trip and the despair of seeing it burn to ash around you. You feel the weight of wisdom of a sun. You feel the universe spinning around you, worshipping you, hating you.

And then you feel nothing at all.

-> death

=survive_bomb

* [Cover your face] You turn away from the distorted air and cover your eyes. # GUIDE kill_krugg/end

- After a moment, with a sickening WHUMP, the air returns to normal.

Krugg's feet still stand on the floor, dribbling ichor, Krugg's wrench lying next to them, a hand still attached.

Also, a tentacle.

~ obj_orks_beaten += 1
~ obj_krugg_dead = true
~ trophy_kill_krugg++
# TROPHY: kill_krugg

-> workshop_empty

= release_Boris

~ boris_chained = false
~ trophy_liberator++
# TROPHY: liberator

You take a key from Krugg's workbench and unlock the chain on the snotling's ankle.

He gives you a big wet kiss, then runs off into the camp, giggling and shrieking.

* [Leave] You leave the workshop.
    -> main

= get_the_thingy

Krugg carefully puts his hand into the jumble of wires in the box, slowly wraps his thick green fingers around the base of the artefact, then violently tugs it out of the box.

You wince, but nothing explodes.

Krugg looks at the artefact for a moment, then puts it down on the workbench and walks off, holding his head and muttering to himself.

* [Pick up the artefact] You reach out to pick up the artefact.  # GUIDE found_artefact/pickup

- As your hand nears it, you hear whispers at the back of your mind. Shadows deepen and the room seems to get darker. A sense of dread floods your stomach.

* [Pick it up] # GUIDE death/krugg_artefact
    As you wrap your fingers around the artefact, you a filled with a sense of vertigo. You close your eyes and try to center yourself.

    Suddenly you cry out from an explosion of pain in your hand - the artefacts burns your fingers. 
    
    * * [Let go] You try and pull your hand away but the artefact must weigh a ton and your skin sticks to it as if melting.
    
    Searing heat races up your arm. You howl in agony as white noise consumes you.

    * * * [Make it stop]

    You hear a voice whisper "Welcome", and then all the lights go out.
    -> death
* [Cover it up]  # GUIDE found_artefact/end # ACQUIRE artefact

~ has_artefact = true
~ trophy_found_artefact++
# TROPHY: found_artefact

You throw a rag over the artefact and the whispers seem to stop.

You pick up the artefact and stuff it into your pocket, then hurry out of the workshop.

-> go_to_main

= accuse

// TODO randomise thise
"So, what do you reckon?" Krugg asks.

* { obj_met_chompa } Was it Chompa?
    "What would he do wiv a snotling?" Krugg scowls.

    "Dat fat git just sits behind is bar all day, drinkin' 'imself stoopid. Nah."
    -> accuse
* { obj_met_wendy } Was it Wendy?
    Krugg scratches his chin.

    "Wendy's a funny old one," he admits. "E's into dis weird foreign fing called, ah, wosseecallit? Onah, dat's it."

    "Besides, he's never in camp when da lads vanish."
    -> accuse
* { obj_met_skullkrusha } Was it Skullkrusha?
    Krugg looks worried.

    "Da boss? Well, if he's pinchin' Krugg's snotlings, dere ent much Krugg can do about it"

    "Dunno why e'd bovver though."
    -> accuse
* { obj_met_sharptoof } Was it Sharptoof?
    Krugg laughs.

    "Haha - yeah, right, like e's gonna cook snotlings fer everyone. Only fing e' wants ta cook wiv is mushrooms. Weirdo."
    -> accuse
+ { obj_clue_chompa || obj_clue_wendy || obj_clue_boris || obj_clue_sharptoof || obj_clue_skullkrusha } "I think it was you, Krugg[."]," you say, carefully. # GUIDE case_closed
    -> accuse_krugg(true)
+ "I don't know[."]," you say.
    "Fat loada use you is," Krugg scoffs.
    -> main_branch

= accuse_krugg(show_intro)

{
    -obj_krugg_investigation_score > 2:
        -> krugg_breaks
}

{show_intro:
-Krugg stares blankly at you. 

"You wot?" he grunts.
}
 
// maybe you have to present enough individual peices of evidence, then when the score is enough, he'll break. Yes I like this

* { obj_clue_chompa } "Chompa says you've changed since you picked up the thingy[."]," you say. # GUIDE case_closed
    ~ obj_krugg_investigation_score += 1

    "Leave the thingy out of dis!" Krugg howls.

    -> accuse_krugg(false)
* { obj_clue_sharptoof } "Sharptoof says you've been torturing snotlings." # GUIDE case_closed
    ~ obj_krugg_investigation_score += 1

    "Den he's a stinkin' liar!" Krugg spits.
    -> accuse_krugg(false)
* { obj_clue_skullkrusha } "Skullkrusha thinks they're dead." # GUIDE case_closed
    ~ obj_krugg_investigation_score += 1

    "Maybe," Krugg nods soberly. "Maybe dey are."
    -> accuse_krugg(false)
* { obj_clue_wendy } "Wendy thinks the warp has got to you". # GUIDE case_closed
    ~ obj_krugg_investigation_score += 1

    "Wendy been spendin' too much time alone in da badlands," Krugg growls.
    -> accuse_krugg(false)
* "Boris is <i>terrified</i> of you[."]," you say." # GUIDE case_closed
    ~ obj_krugg_investigation_score += 1

    "Dat lil grot's scared of 'is own shadow" Krugg scoffs.
    -> accuse_krugg(false)
+ "Nah, forget it"
     -> accuse

// dropping third person feels more personal now. he's not hiding.
= krugg_breaks

* And just like that, Krugg breaks # GUIDE case_closed/end

-
~ trophy_case_closed += 1
# TROPHY: case_closed

Krugg stares at the floor, wringing his hands.

"Maybe you're right," Krugg says, quitely. "I don't remmeber doin it. But..."

Krugg seems to wrestle within himself.

"Dere are monsters inside my head," he says, quietly. "Dey make me do fings. Un-orky fings."

* Like what?
* Like murder?

- "Dunno," he grunts.

"Sometimes I does somethin' and... I tries to forget after. An' I'm gettin' good at it."

"I lose days, sometimes."

-> confession_menu

= confession_menu 

* "I thought orks enjoyed violence?" you say.
    "Violence? Yer!" Krugg growls, setting his jaw.
    
    "We boyz loves fightin', scrapping, shootin', blowing stuff up. Dats good propah orky ennertainment."
    
    "But cold-blooded murder? Torture?" Krugg shakes his head. "Dat ent da orky way."
    -> confession_menu

* "Why don't you just stop?" you say.
    "Can't," Krugg grumbles.

    "I don't want to do it. Deses forts, dey ent my own. But dey stronger than me."

    "Sometimes I says: 'OK Krugg, no more, dat's enough'. But den... den the forts come back."
    -> confession_menu

* "You should give me the thingy." # GUIDE woo_krugg/start
    -> give_the_thingy

* "I've heard enough[."]," you say. And you mean it.
     -> main_branch

= give_the_thingy 
    Krugg stares aghast at you.

    * "It's the only way to quieten the monsters[."]," you say.

    - Krugg nods, slowly.

    "Yer," he whispers. "Da monsters."

    Krugg walks over the the shrine and tears down the curtain.

    * * [Wait quietly] You are silent while <>
        -> get_the_thingy
    * * [Encourage him] With a nod of encouragement, <>
        -> get_the_thingy

= workshop_empty

{ boris_chained:
    // First time should always be immediately after the bomb, so it's a bit special
    Boris the snotling {squeals with joy, rattling his chains|sits in the corner, picking his nose}.
    * [Release Boris] # GUIDE liberator/end
    -> release_Boris
 -else: 
    The warehouse is silent and empty.
}
// TODO does this make sense? If krugg dies, what happens to the box?
// { has_artefact==false:
    // + [Investigate the glowing box] You see a glowing box on a shelf behind Krugg's workshop.

    // As you move closer, you hear whispers at the back of your mind. They tell you to take the box.

    // The box is rectangular, covered in spikes and giving off a curious purplish light. You find it to be strangely beautiful.

    // + + [Take the box] You are compelled to pick up the box.

    // You put a hand on the box. It is cold to touch - then, suddenly, it is boiling. You release your hand and hear a sudden unearthly scream. You cover your ears but it doesn't seem to help.

    // You fall to your knees and the screaming continues.

    // + + + [Stand up] You try to stand, but your legs don't seem to respond.

    // You look down and see a mass of writhing tentacles where your legs should be.

    // There's a blinding flash of light, then a surge of brilliant pain which lasts forever.
    // -> death
// }
{ has_armour==false:
    * [Collect the armour] You pick up the heavy armour from the floor. # GUIDE found_armour/end
    ~ has_armour = true
    # TROPHY: found_armour
    ~ trophy_found_armour++
    It's rather large on you, but it does fit. You feel a little bit safer.
    
    -> workshop_empty
}
 -> go_to_main

= start_investigation

Krugg whips his head up and locks smouldering eyes to yours.

"Whaddaya know 'about that?"

* "It was obvious from context[."]," you explain.
* "Only what you just told me[."]," you suggest.
* "Nothing[."]," you shrug.

- -> start_investigation_1

= start_investigation_1

{ once:
    -Krugg growls and glares towards the end of his workbench.

    "Told ya someone would get nosy," he mumbles under his breath.

    You see nothing there but a pile of junk and a heavy black curtain.

    His eyes snap back to you, filled with rage, then wide with pleading. He looks at the floor.

    "Krugg needs grots ta keep dis place running."
}

* "So where are they going?"[] you ask.
    "If Krugg knew that," he growls. "Krugg would bring dem back."

    He looks over at the curtain again.

    -> start_investigation_1
   
* "How many have you lost?"[] you ask.
    "Loads."
    -> start_investigation_1

* "What do you need them for?"[] you ask.
    "All sorsta stuff," he shrugs.

    "Da grots fetch me stuff, clean stuff, test stuff out."

    He looks desperately sad for a moment.

    "Kompany," he adds, in a tiny voice.
    -> start_investigation_1

// default choice when all others are exhausted
* ->

- Krugg mutters to himself, eyes flicking between the curtain and you.

~ enable_curtain_discussion = true
~ obj_krugg_investigation_started = true

-> main_branch

= reveal_shrine

{ once:
- Krugg's eyes light up, darting between the heavy black fabric and you.

You get a strange sense that something is wrong in that corner. Like the shadows are too dark.

Krugg licks his lips and grins.

"You ready ta see me fingy?"

Before you can answer, he pulls back the curtain.

A step ladder stands adorned in thick, flaming candles. Some kind of artefact, in the shape of a crucifix, stands on the top, strapped to some kind of device. An unblinking eye stares from the cross.


- Krugg pulls back the curtain once more, revealing the Shrine to the Bigg Spooky Bombb.
}
-> describe_shrine_menu

= describe_shrine_menu

+ { explain_artefact > 0 } "How can I help?"[] you ask. # GUIDE kill_krugg
     -> bomb_explain
* [Examine the artefact] # GUIDE kill_krugg
    -> examine_artefact
* { examine_artefact > 0 } "So what is it?"[] you ask.
    -> explain_artefact
* { examine_artefact > 0 } "Can I have it?"[] you ask.
    Krugg looks at you in horror.

    "Absolutely not!" he barks.

    -> describe_shrine_menu
+ [You've seen enough] You leave the shrine alone for now.
    -> main_branch

= examine_artefact

The artefact is bolted to a metal box, tubes of some kind of glowing liquid looping around it. Nails and bolts pepper the surface like iron growths.

The artefact itself stands tall from this mess, a golden crucifix with uneven axis. Peculiar glyphs adorn the surface like tattoos, and a painting of an eye sits in the centre, looking down at you.

* [At least you think it's a painting.] It's a painting, right?

- You suddenly feel like you are being watched.
* "Did you hear that?" [] You ask, looking over your shoulder.

You are sure you can hear someone whispering.

Krugg doesn't reply. He stands staring at the artefact, his lips moving mutely.

-> describe_shrine_menu

= explain_artefact

~ persist_krugg_likes_bombs = true

"Dis is me Big Spooky Bombb!" Krugg says excitedly, wide eyes whipping around to meet yours.

* "I've seen bigger[."]," you say.
    Krugg looks offended for a moment.

    "It's small, but tuff," he says, tapping it with a thick finger.

    "At least, dat's what da voices say."
* "Spooky?" []you ask.
    ~ persist_krugg_dislikes_whispers = true
    "It speaks sometimes," he says, quietly. "Tells Krugg to do fings. Un-orky fings."
* "It's pretty[."]," you say.

- The ork suddenly looks uncomfortable.

"Troof is," he says quietly. "Krugg ent sure wot it is."

* "Is you fick?"
* "On the grog, eh?"

- Kruggs eyes drop to the floor.

"Krugg found dis weird... fing when we raided some warpy gitz. Dey were all wearin' dresses and had purple glowy eyes - very squishy.

"Dis fingy... well it's clearly special. So Krugg thinks, why not strap a nice bomb to it? Nice little bomb on a nice little fingy."

Krugg stares into the eye.

"'Ere," Krugg says, seemingly remembering you're there. "You any good wiv bombs? Maybe you help old Krugg?"

-> describe_shrine_menu

= chat_to_boris

{ stopping:
    -You approach Boris, chained to a workbench at the far side of the workshop.

    He picks his nose absent mindedly, watching Krugg. You try and catch the snotling's attention, but he looks around you to stare at Krugg.

    -Krugg walks over towards you. As he does so, Boris scurries behind a toolbox, peeking out at the ork. Krugg picks up a steel rod from the floor, then heads back to his workbench.

    - ~obj_clue_boris = true
    
    Boris's eyes never leave Krugg. He's either in love with the big ork, or completely terrified of him.
}

* "You OK buddy?" # GUIDE case_closed
    The snotling looks at you a moment, then resumes staring at the ork.

    -> chat_to_boris
* "Where's your mates?" # GUIDE case_closed
    The snotling's eyes swivel towards you, then lock back onto Krugg.
    -> chat_to_boris
+ [Go Back] You leave the snotling alone.
    -> main_branch

= chompa

"You wot?" Krugg splutters. "Dat piss 'ed stole my juice? To make stoopid grog?!"
 
Krugg paces around his workshop angrily.

"Dat fickhed! That filfy grot! Dat squig tickler!"

Krugg kicks over a bucket full of bolts. Boris squeals excitedly, rattling his chains.

* "You shud go teach him a lesson["]," you suggest.
    "Yer!" Krugg hisses, clenching his fist.
* Say nothing

- Krugg fixes you with a furious gaze, then storms over to a pile of junk.

He tosses various scraps of metal aside, clanging and crashing. Then he shrugs a heavy machine gun onto his shoulder.

"Right," Krugg growls. "Let's go 'av a word."

* [Go to Chompa's bar]
    -> Chompa.krugg