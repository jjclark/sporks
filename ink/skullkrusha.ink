/*
 Skullkrusha is the ork Warboss.
 
 He's building up a Waaargh - a horde of orks which will sweep over the sector, ruining everything.
 
 When you learn this (it'll just happen after a bit of chat), you can choose to asassinate him
 You can opt to be on a mission on assassinate him and stop the waargh. Or, you can become his right-hand-ork if you gain his trust.

 If you have the chaos artefact, you can give it to him and trigger the warp

 If you give him real squig surprise or armour, you can join the krew and be friends
     - Can Chompa give him something? Then every ork has a role in befriending SK
     - Maybe you learn a secret at the end of all chompa's dialog
     - and maybe you have to stop the drinking game when you learn it, or you forget
     - Ok, so that's kinda cool, but maybe quite difficult for an optional quest?
     - players won't see that all orks lead to unlocking SK. It's nice design-wide but redundnat really.
*/

// more like trust. Or time passing.
VAR popularity = 0
VAR flattery = 0

VAR persist_skullkrusha_likes_meat = false
VAR persist_skullkrusha_likes_gear = false
VAR persist_skullkrusha_dislikes_flattery = false
VAR persist_skullkrusha_dislikes_romance = false
VAR persist_skullkrusha_dislikes_himself = false

== function status_skullkrusha ==
{
    - obj_met_skullkrusha:
        ~return "Skullkrusha is shouting at some orks"
    -else:
        ~return "The huge ork warboss is shouting at some orks"
}

== Skullkrusha

= profile

WARBOSS SKULLKRUSHA

Likes:
    \* { -persist_skullkrusha_likes_meat:
        <> Meat
        -else: <> ???
        }
    \* { -persist_skullkrusha_likes_gear:
    <> Gear
    -else: <> ???
    }

Dislikes:
    \* { -persist_skullkrusha_dislikes_flattery:
    <> Flattery
    -else: <> ???
    }
    \* { -persist_skullkrusha_dislikes_romance:
    <> Romancy stuff
    -else: <> ???
    }

+ [<< BACK] -> main

= start

{
-obj_met_skullkrusha == false:
    ~ obj_met_skullkrusha = true
    
    The Warboss is the biggest ork you've ever seen. Skullkrusha stands a clear head taller than any other ork in the camp.
    
    Plates of heavy armour cover his bulky body. One arm ends in a huge power klaw, pistons audibly grinding, while a jagged chinguard exaggerates his slathering jaws.

    He is shouting at the cluster of nervous-looking orks who follow his every step.

    "You got no grub, you got no gear, you got no clue!" he roars. "Get outta my sight!"

-skullkrusha_done:
    The warboss struts around the camp, shouting instructions.

    -> done_branch

-else: 
    Skullkrusha crashes through the war camp, demanding food and gear and pushing the other orks around.

    // TODO write this in a bit more
    A small entourage of ork nobs follow him, taunting and jeering anyone who gets in their way.
}

+ "Oy, boss."
    -> main_branch
+ [Come back later] You leave the warboss to... whatever he's doing.
    -> main


= main_branch_debug

// ~has_stew = true
~has_secret = true
~has_armour = true
~flattery = 1
~popularity = 2

-> main_branch

= main_branch

-> main_branch_intro -> 

* { obj_krugg_investigation_started } "What do you know about Krugg's snotlings?"[] you ask. # GUIDE case_closed
    -> investigation

* { has_secret } "'I know your secret[."],"  you say, holding the Warboss' gaze. # REQUIRE secret # GUIDE woo_skullkrusha/secret
    -> secret

* { has_stew } "'Got some stew for ya." # REQUIRE stew # GUIDE woo_skullkrusha/stew death/sk_no_meat
    -> feed_skullkrusha

* { has_meat_stew } "'Got some good meaty stew for ya."  # REQUIRE meat_stew # GUIDE woo_skullkrusha/meat_stew
    -> feed_skullkrusha_meat

* { has_armour } "Found you some nice gear."  # REQUIRE armour # GUIDE woo_skullkrusha/gear
    -> gear_skullkrusha

* { popularity > 1 } "What's dat about a Waargh?" # GUIDE mission_accomplished/start
    -> enable_assassination

* { flattery == 0 } "Nice Klaw[."]," you say approvingly.
    -> flattery_2

* { flattery > 0 } "You're ded 'ard[."]," you croon.
    -> flattery_1

* { flattery > 1 } "Fancy a walk, boss?"
    -> maybe_a_walk

* { flattery > 2 } "I like your little hat" # GUIDE death/sk_flattery
    -> nice_hat

+ { has_artefact && flattery > 0 } "'Ere boss, wanna see my thingy?" # REQUIRE artefact # GUIDE warp_waargh/start
     -> start_warp

+ ["Nuffink."]"Uh, nuffink, boss," you say.
    + + [Go back]
        -> main

= main_branch_intro

{
    - main_branch == 1:
        Skullkrusha turns on you, eyes full of rage and hunger and probably death.

        "Whaddayoo want?" he demands.

        "You betta have somethin' good fer me, or I'll rip yer guts out."
    - popularity == 2:
        "Oi!" Skullkrusha barks over your shoulder, where squabbling orks are clustered around a Wartrakk.
        
        "Come on you runts, we ain't never gonna get dis Waargh going if you don't pull yer fingers out."

        The squabbling intensifies. Skullkrusha's gaze returns to you.
    - flattery == 2:
        ~ persist_skullkrusha_dislikes_flattery = true
        Skullkrusha gives you a suspicious look.

        {"You must be da one who's been chattin' up me boyz. Well I ent 'avin it.'"|\ }

        "Ya wanna be part of my klan, ya gotta be useful or be gone, geddit?"
    -else:
        Skullkrusha fixes eyes full of {~hate|rage|fury|hunger|contempt} upon you.
        
        {~"Wot?"|"Whaddaya want?"|"Dis betta be good."}
 }

->->

= flattery_1

~ popularity ++
~ flattery++

"Yes I am!" roars Skullkrusha.

"Watch me kill this grot," he says, turning on a passing ork.

Skullkrusha wraps the power klaw around the unfortunate ork's head before he realises what's going on. His skull pops like watermelon and the ork collapses to the flow in a shower of gunk.

"Haw! Classic!" the Warboss roars.

* [Laugh] You laugh at the stupid dead ork.
* "Good one boss[."]," you creep.

- -> main_branch

= flattery_2

~ popularity ++
~ flattery++

Skulllrusha lifts his power klaw and examines it, beaming.

"I love me a nice bit of gear, and dis is da best in da sector," he purrs.

"I call it Cuthbert. Coz it's a killing machine, see?"

* "Dat's pretty clever, boss[."]," you lie.
    ~ persist_skullkrusha_dislikes_flattery = true
    Skullkrusha's face falls.

    "Flattery is fer pinkskinz," he growls.
* "Uh, sure boss[."]," you say uncertainly

- -> main_branch

= done_branch

* "Are we ready to Waargh!"
    "You bet we are!" Skullkrusha cries, snapping his monstrous power klaw.

    "WAAAARGH!" he screams.

    * * "Waaaaargh!"[] you cheer.

    -> trigger_waargh

+ "Got any jobs wot need doin?"
    -> hang_out
-> go_to_main

= hang_out

"I got work if you got the gutz," Skullkrusha says.

"Dese boyz dunno their arses from dere glasses," he growls.

You spend the afternoon helping Skullkrusha with various odd-jobs in preperation for the Waargh.

-> go_to_main

= gear_skullkrusha

# USE_ITEM: armour

"Got some gear for ya," you say, handing over the breastplate to the warboss.

"Not bad," Skullkrusha says, turning it over in his klaw. "Too small fer me, but good enuff fer Toerag."

Skullkrusha turns on the pack of ork nobs behind him. They flinch backwards.

"'Ere, Toerag'" Skullkrusha shouts, throwing the breastplate into the pack of orks. "Try dis on fer size."

Skullkrusha returns his indomitable gaze to you.

-> join_krew

= secret

# USE_ITEM: secret

"Ya wot?" Skullkrusha roars. "I never touched dem fish!"

* [No, not that secret.] "Um, no, not that secret," you splutter.

- "I know how scared you are. How much you despise yourself. How your anger is a shield."

Skullkrusha stares at you, eyes smouldering, klaw clenching.

* [Wait for inevitable death] You tense up, waiting for Skullkrusha's rage to manifest itself into your pain. # GUIDE death/skullkrusha_fake

- The warboss laughs.

"Dat's FUNNY!" he bellows.

Skullkrusha whips his head back and howls with joy. He smacks his thighs and his chest, his shoulders shaking. The orks around him start to laugh nervously.

* [Haha, yeah] You join in the laughter awkwardly.

- Finally he starts to quiten down, drawing in deep, wheezing breaths as his breaking settles.

"It's funny," he says, wiping his eyes with the klaw, "'Cos it's true."

-> join_krew

= feed_skullkrusha

# USE_ITEM: stew

~ popularity ++

"What's dis?" Skullkrusha seizes the bowl and sniffs at it.

* "Grub."
* "Squig surprise."

- "It's not from that poncy Sharptoof is it?"

Skullkrusha takes a sniff, then tosses the bowl to the side angrily.

"I ain't eating this gruel! It's got no meat in it!"

~persist_skullkrusha_likes_meat = true

In a fit of rage, Skullkrusha punches a nearby nob with a claw, sending him hurling across the camp.

"I oughta kill that runt!"

* Cower in fear
* "Sorry boss!"
* Do nothing

- "But he's all da way over dere! An' I'm all da way over here!" Skullkrusha rages, stamping on the floor.

"Reckon I'll kill you instead!!" Skullkrusha roars.

He hurls the hot stew in your face. You recoil away instinctively. Then Skullkrusha's klaw punches deep into your belly.

You look into Skullkrusha's malevolent eyes as he opens the klaw, disembowelling you.

The soup bubbles in the dirt.

-> death

= feed_skullkrusha_meat

# USE_ITEM: meat_stew

"What's dis?" Skullkrusha seizes the bowl and sniffs at it.

* "Squig surprise[."]," you say.

- "With real squig!" you add hastily.

Skullkrusha growls and tips the bowl into his mouth. He chews slowly, brownish juice dripping down his chin.

"Not bad," he rumbles. "You can taste da squig, but it's still tasty!"

* "Phew!" [] you say.
* "Yer welcome[."]" you say.

- -> join_krew

= maybe_a_walk

"A wot?" Skullkrusha says.

"You wanna 'old 'ands, do yer? Go dancin'? Mebbe eat a nice SALAD?"

* "Actually, yes that sounds lovely." # GUIDE death/sk_walk
    ~ persist_skullkrusha_dislikes_romance = true
    "Yerr," Skullkrusha says quietly. "'Old my 'and then."
    
    Skullkrusha slowly reaches out with his power klaw - then jerks forward and chops your arm clean off.
    
    You scream in horror and fall to your knees as the Warboss laughs.
    
    "Haw haw haw!" he laughs. "Look at dat boyz! 'E's armless now!"
    
    * * "Aaaaaah!!!"
    * * "Unholy squig offal! The pain!"
    * * ...

    - - "Armless!" Skullkrusha shouts gleefully as he swings his klaw around again. The next thing you know is darkness.
    -> death

* "Nar, just pullin' yer leg!"
    Skullkrusha growls and flexes his klaw.
    -> main_branch

= nice_hat

"Dat's it!" Skullkrusha roars. "I've 'ad enuff of your sweet talkin' and gabbin'!"

Skullkrusha lashes out with his klaw and seizes your body.

* [Break free]

You struggle helplessly as he lifts you off the floor and effortlessly snaps you in half.

His brash laughter rings through your mind as your awareness fades.

-> death

= enable_assassination
"We'z buildin a Waargh!!" Skullkrusha explains, warming to his subject.

"Dis will be dat biggest ork horde da sector's ever seen! We gon' make Thraka look like weedy grot. Da green tide rises!"

"Get! Ready! tooooooo!" Skullkrusha takes a deep breath.

"Waaaaaaaaaaaaargh!" he roars.

* "Target identified. Killmode engaged." # GUIDE mission_accomplished
    -> expose_truth
* "Waaaargh!"[] you roar in reply.
    ~ popularity++
     "WAAAAAAARRGH!" Skullkrusha screams back at you.

     -> main_branch

= expose_truth

This is it.

This is the ork you've been looking for all this time.

This is the source of the next Waargh!, the greatest threat to the system in ten generations.

* "Prepare to die, Xeno[."]," you say.

- Skullkrusha stares at you, slack-jawed. He looks at his nobs. They each shrug in turn.

Skullkrusha doubles over in great, hawking laugher.

* "Initiating self-destruct in[..."]," you begin to count.

Skullkrusha's laughter stops abruptly.

* * "5."

The Warboss charges towards you.

* * * "4."

The Warboss barges a burly nob out of the way as he closes the distance.

* * * * "3."

Skullkrusha reaches out with his klaw and grabs you.

* * * * * "2."

The power klaw squeezes around your waist and raises you from the floor.

* * * * * * "1."

Skullkrusha brings you level with his face and roars, his breath rank in your face.

* * * * * * * [Kaboom!] You explode. # GUIDE expose_truth/end

The crater you leave is some 20-metres in diameter, leaving half the Ork encampment in a smoking ruin, the earch scorched, littered with charred bodies.

As you consciousness fades, you are satisfied that your work is completed.

There's one less Ork warlord to terrorise the sector.

* * * * * * * * [Next]
        ~ trophy_mission_accomplished++
        # TROPHY: mission_accomplished
        # ENDING: boom.jpg In the grim darkess of the future, there will be a little less war.
        -> fin_good

= join_krew

"I could use a good ork like you," Skullkrusha muses. "Yer krafty and yer tuff. I like dat."

"'Ow about you join my krew?"

* "Ok[."]," you grunt. # GUIDE woo_skullkrusha/end
* "Nah fanks[."]," you grunt. # GUIDE death/sk_rejection
    // TODO come back to this a bit later
    Skullkrusha recoils.

    "Fine," he grunts, turning away.

    "Kill 'him boyz. If 'e's got any gear, it's yours"

    Skullkrusha walks away a the pack of nobs surges towards you, weapons bristling.

    You try to run but an axe-blade between your shoulders stops you in your tracks.

    -> death

- Skullkrusha slaps you on the back.
~ skullkrusha_done = true
~ trophy_woo_skullkrusha++
# TROPHY: woo_skullkrusha

"Right den - welcome to da krew!" he beams.

Suddenly, his face twists with rage.

"Now you just gonna stand around or you gonna help me get dis Waarg going you stinky squig-arsed runt?"

* "Waaaargh!" # GUIDE something_waargh/start

    Skullkrusha joins you in a mighty roar.

    "Dat's da spirit!" Skullkrusha growls.
    
    You both howl into the sky, whipping the orks around you into a frenzy.

    * * "Waaaargh!"

    "You're right!" Skullkrusha roars. "Let's ged on wiv it!"

    * * * [Launch the Waargh!] # GUIDE something_waargh/end
        -> trigger_waargh
    * * "Okay." -> just_be_friends

* "Dat's great, but I got stuff to do."
    -> just_be_friends

= just_be_friends

You throw Skullkrusha a sloppy salute and head back into the camp.

-> go_to_main

= trigger_waargh

# AUDIO: ending-bad

Over the next days and weeks, you help Skullkrusha out-fight and out-wit the other ork klans into his own horde.

When the Waargh finally explodes into the system, Skullkrusha's orks leave death and devastation in their wake.

You fight, kill and drink by Skullkrusha's side.

* [Next]
    ~ trophy_something_waargh++
    # TROPHY: something_waargh
    # ENDING: waargh.jpg In the grim darkness of the future, there is only WAARGH!
    -> fin


=start_warp

# USE_ITEM: artefact

"Whaddaya mean, 'thingy'?" Skullkrusha asks.

You pull a layer of cloth away from the artefact and show it to Skullkrusha. The warboss is bathed in strobing purple light.

The sky seems to dim as a rush of whispering voices fills the air.

"Dat... it's..." Skullkrusha stammers.

+ "See? It's a thingy[."]," you say.

- "Can... can I hold it?" Skullkrusha pleads quitely.

He fixes a trembling eye on you.

"Please?"

You feel a strange mixture of dread and excitement as you weigh the decision in your hand.

+ "Of course - it's for you." # GUIDE warp_waargh

    Skullkrusha's eyes light up. He reaches out and delicately plucks the cross from your hand.

    The whispers fall quiet for a moment - then all goes pitch dark. You hear a confusion of grunts and shouts around you.

    Two purple pools of light burn from the darkness. Skullkrusha's eyes.

    * * [Time to leave]
        Breathless, you take a step back. You fear you have made a terrible mistake.
    * * [Stick around]
        You smile as the power of the warp seeps into Skullkrusha's veins.

    - - Light returns to the sky, revealing Skullkrusha's malevolent grin. Thin entrails of black smoke whirl around his body, streaming from his eyes like a fire.

    Skullkrusha starts to laugh, a low chuckle thick with evil intent.

    "Dey ent never seen a Waargh like this," he rumbles.

    Skullkrusha raises his arms and howls. With a rush of air and a flash of light, a portal opens up behind him: a whirling, purple and black vortex of electricity and shadow.

    * * [Welcome Chaos]

    - - The light begins to fade again as strange and terrifying shapes begin to creep from the portal.

    Around you, orks are enveloped in purple light and black shadow, and burst into terrible new forms, howling with pain and rage and bloodlust.

    The whispers settle in your mind and turn to clear commands. Tentacles and spines burst from your flesh. You welcome them, just as you welcome the darkness that consumes you from the inside.

    The daemon princes of the warp anoint you as a servant of chaos.

    * * [Accept your fate] # GUIDE warp_waargh/end

    # AUDIO: ending-bad

    - - The warp waargh floods the sector like a burst sewage main. The orks, corrupted by Chaos and in league with demons of the warp, set a fire the likes of which have never been seen.

    The Imperium is quickly overrun by the tide of evil as cities are razed to the ground. Then, with nothing left to maim, the orks and daemons turn on each other.

    * * [Next]
        ~ trophy_warp_waargh++
        # TROPHY: warp_waargh
        # ENDING: chaos-waargh.png In the grim darkness of the future, you have found only chaos and pain.
        -> fin

+ "I don't think that's a very good idea[."]," you say, covering up the artefact.
    Skullkusha blinks then scowls, his face twisting with rage.

    "Fine! I wants grub anyways, not trinkets!" he shouts.
    -> main_branch

= investigation

"What?" Skullkrusha grunts, cocking his head. "Whaddo I care about some dead snotlings?"

"Just tell 'him to grab some new ones and get on wiv my gear."

~ obj_clue_skullkrusha = true
-> main_branch