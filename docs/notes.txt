Beta stuff

 * Is beating 1 ork before sharptoof and 2 before skullkrusha too hard?
     - 
* should orks_beaten be objective?
     - means you can die after speaking to an ork and still acces skullkrusha
     - ok I think I'll try this

TODOs before 1.0

* When you visit a dead ork, we need some kind of ghost representation
     - the easiest way is to just make them transparent...
* If I'm gonna beta test it, update the guide
* Objective/subjective state - get it right
    - it's not too bad because when you die, nothing resets
    - there will be bugs in here but can I reeeeallly be arsed?


Ok so lets get back into this

I think there are a couple of major krugg things here

* Add the investigation stuff that breaks krugg and makes him give you the artefact
    - this also lets you woo krugg by talking to him
* Update the bomb sequence so that you never get the artefact from it (krugg dies)
     - you can still free boris (and maybe we can make that more interesting) and get teh armour
     - need to introduce the artefact properly, I think I was working around that

And then we playtest through it as much as I can bear.


I am using a day of holiday for this and really I'd like to get something good out of it.

starting early and getting a chunk done should hopefully mean I'm not too intimidated by it all.

Get this all done and I can waste time with a clean conscience



--

idea

what if I massively simplify the main gallery?

I don't think the profile adds much these days, and the way the game is now structured I don't think you need those hints to get throug the puzzles

so:

 - click once on the ork to embigged
 - we show the name, role, status and a big "chat" button
 - this pops up in some kind of box that uses the triangles from the gallery, maybe
 - bespoke text? Can I do it?

Doing it this way, I don't need to worry about a big panel that has to fit on the screen



## objective / subjective stuff

- when you die, skullkrusha flattery state should reset

ok so what I can do is say: on death, reset any variables starting with subj_.

But what does reset mean? hmm.

Maybe it's easier for every character to have like a reset function? oR could I just set every var to 0?

A slightly wider take on that is that all story counts have to reset

Ok, what if I flip it? On death, we reset all progress EXCEPT for objective stuff? Now all visit counters reset, but I can fix certain bits of state

