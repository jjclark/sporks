const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');


module.exports = (env = {}) => {
    const isProd = env.prod;
    
    return {
        mode: isProd ? 'production' : 'development',
        entry: path.resolve(__dirname, 'src', "index.js"),
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'bundle.js'
        },
        optimization: {
            minimize: isProd
        },
        resolve: {
            extensions: ['.js', '.jsx', '.json'],
        },
        devServer: {
            hot: true,
        },
        module: {
            rules: [{
                test: /jsx?$/,
                exclude: /(node_modules)/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react'],
                        plugins: isProd ? [] : ['react-refresh/babel']
                    }
                }]
            }, {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            }, {
                test: /\.(png|jpe?g|gif)$/i,
                use: [{
                    loader: 'file-loader',
                }],
            },]
        },
        plugins: [
            !isProd && new ReactRefreshWebpackPlugin(),
            new CleanWebpackPlugin(),
            new CopyPlugin({
                patterns: [
                { from: 'static', to: '.' },
                { from: 'img/scaled', to: 'img/scaled' },
                { from: 'img/trophies', to: 'img/trophies' },
                { from: 'img/endings', to: 'img/endings' },
                { from: 'img/ui', to: 'img/ui' },
                { from: 'lib', to: '.' },
                ],
            }),
        ].filter(Boolean),
    }
};