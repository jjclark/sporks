import shuffle from 'shuffle-array';

const artefactLines = [
    'I see you. I hear you. I need you. Come to me.', 
    'Can you feel the darkness inside you?', 
    'Embrace the warp. Feed the warp. Be the warp.',
    'They will betray you.',
    'I know your secret. You secret is me.',
    'Kill kill kill kill kill',
    'They can never love you like I can',
    'Something dark is coming. Something terrible.',
    'They whisper your name in the halls of the dead.',
    'Come with me and I will make you mine.',
    'I can taste your suffering, and I like it.',
];

export default [
    {
        id: 'armour',
        name: 'Armour',
        description: 'A fine armoured breastplate made by Krugg',
        icon: 'armour',
    },
    {
        id: 'stew',
        name: 'Stew',
        description: 'A mushroomy stew made by Sharptoof. Needs salt.',
        icon: 'stew',
    },
    {
        id: 'nickleback_stew',
        name: 'Nickleback Stew',
        description: 'A delicious meaty stew made by Sharptoof',
        icon: 'meat_stew',
    },
    {
        id: 'artefact',
        name: 'An Artefact',
        description: () => `${shuffle.pick(artefactLines)}`,
        icon: 'artefact',
    },
    {
        id: 'money',
        name: 'Some Money',
        description: 'A few coins',
        icon: 'money',
    },
    {
        id: 'secret',
        name: 'A Secret',
        description: 'You know Skullkrusha\'s secret',
        icon: 'secrets',
    },
    // This bloats the guide way too much, so I'm gonna drop it
    // {
    //     id: 'respect',
    //     name: 'Wendy\'s Respect',
    //     description: 'Hard-earned, too',
    //     icon: 'wip',
    // },
    {
        id: 'meat',
        name: 'Meat',
        description: 'A chunk of fresh Nickleback meat. Well, fresh-ish.',
        icon: 'meat',
    },
];