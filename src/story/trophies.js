// TODO are the names or descriptions spoilers?
// Something Waargh maybe gives away that ending a bit?

// https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=64B5F6
// 64b5f6

const colors = {
    BLUE: '#64b5f6',
    YELLOW: '#fff176',
    PEACH: '#ff8a65',
    GREY: '#90a4ae',
    PURPLE: '#ba68c8',
    GREEN: '#388e3c'
}

export default [
    // BROMANCE
    {
        title: 'Woo Krugg',
        description: 'You befriended Krugg, the mechanic',
        name: 'woo_krugg',
        group: 'Bromance',
    }, {
        title: 'Woo Chompa',
        description: 'You befriended Chompa, the bartender',
        name: 'woo_chompa',
        group: 'Bromance',
    },
    {
        title: 'Woo Wendy',
        description: 'You befriended Wendy, the hunter',
        name: 'woo_wendy',
        group: 'Bromance',
    },
    {
        title: 'Woo Sharptoof',
        description: 'You befriended Sharptoof, the chef',
        name: 'woo_sharptoof',
        group: 'Bromance',
    },
    {
        title: 'Woo Skullkrusha',
        description: 'You befriended Skullkrusha, the warboss',
        name: 'woo_skullkrusha',
        group: 'Bromance',
    },



    // ENDINGS
    {
        // sharptoof
        title: 'Something More',
        description: 'You found the pale light of love in the grim darkness',
        name: 'something_more',
        group: 'Big Finales',
    }, {
        // skullkrusha
        title: 'WAARGH',
        description: 'You joined the Waargh!',
        name: 'something_waargh',
        group: 'Big Finales',
    }, {
        // skullkrusha
        title: 'Mission Accomplished',
        description: 'You assassinated your target on behalf of the emperor',
        name: 'mission_accomplished',
        group: 'Big Finales',
    }, {
        // skullkrusha
        title: 'Bad Boyz',
        description: 'You succumbed to the lure of Chaos',
        name: 'warp_waargh',
        group: 'Big Finales',
    }, {
        // everyone
        // This one won't have an explicit guide (maybe it needs a clearer title?)
        title: 'Everybody\'s Best Friend',
        description: 'You became friends with everybody',
        name: 'friendzone',
        group: 'Big Finales',
    },

    // INVENTORY
    {
        title: 'Find the Armour',
        description: 'You found some neat armour',
        name: 'found_armour',
        group: 'Gear',
    },
    {
        title: 'Find the Artefact',
        description: 'You found a spooky artefact',
        name: 'found_artefact',
        group: 'Gear',
    },
    {
        title: 'Find some Meat',
        description: 'You found some meat',
        name: 'found_meat',
        group: 'Gear',
    },
    {
        title: 'Find a Secret',
        description: 'You learned Skullkrusha\'s secret',
        name: 'found_secret',
        group: 'Gear',
    },
    {
        title: 'Find some Money',
        description: 'You found a few coins',
        name: 'found_money',
        group: 'Gear',
    },
    {
        title: 'Find Stew',
        description: 'You found some \'orrible stew',
        name: 'found_stew',
        group: 'Gear',
    },

    // ORKY STUFF
    {
        // krugg
        title: 'Bomb squad',
        description: 'You set off the spooky bomb',
        name: 'kill_krugg',
        group: 'All da rest',
    },
    {
        // krugg
        title: 'Case Closed',
        description: "You found out what happened to Krugg's snotlings",
        name: 'case_closed',
        group: 'All da rest',
    },
    {
        // chompa
        title: 'Bottoms Up',
        description: 'You drank an ork under the table',
        name: 'bottoms_up',
        group: 'All da rest',
    }, {
        // chompa
        title: 'Secrets',
        description: 'You learned Skullkrusha\'s deepest secret',
        name: 'secrets',
        group: 'All da rest',
    }, {
        // chompa
        title: 'Penny for da greens',
        description: 'You tipped the greens-playing gretchen',
        name: 'tip',
        group: 'All da rest',
    }, {
        // krugg / chompa
        title: 'Orkicide',
        description: 'You drove an ork to murder',
        name: 'orkicide',
        group: 'All da rest',
    }, 

    {
        // Wendy
        title: 'Pleased To Meet You',
        description: 'You learned Wendy\'s name',
        name: 'introduce_wendy',
        group: 'All da rest',
    },
    {
        // krugg
        title: 'Liberator',
        description: 'You freed Boris the snotling',
        name: 'liberator',
        group: 'All da rest',
    }, 
        {
        // krugg
        title: 'Case Closed',
        description: 'Solve the case of Krugg\'s disappearing snotlings.',
        name: 'case_closed',
        group: 'All da rest',
    }, 
    {
        // any
        title: 'A Good Day To Die',
        description: 'You died',
        name: 'death',
        color: colors.GREEN,
        group: 'All da rest',
    }, {
        // Wendy
        title: 'Way of Wendy',
        description: 'You mastered the hunt, ork style',
        name: 'way_of_wendy',
        color: colors.YELLOW,
        group: 'All da rest',
    }
];