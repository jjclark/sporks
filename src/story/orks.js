import kruggPng from '../../img/scaled/krugg-1000x.png';
import chompaPng from '../../img/scaled/chompa-1000x.png';
import wendyPng from '../../img/scaled/wendy-1000x.png';
import wendy2Png from '../../img/scaled/wendy2-1000x.png';
import sharptoofPng from '../../img/scaled/sharptoof-1000x.png';
import skullkrushaPng from '../../img/scaled/skullkrusha-1000x.png';

export const orks = [{
    name: 'Sharptoof',
    path: 'Sharptoof.start',
    role: 'The Cook',
    order: 2,
    img: sharptoofPng,
    unlock: (story) => {
        // TODO this is duplicated from the game. Not crazy about this.
        const { obj_orks_beaten, persist_sharptoof_dislikes_betrayal } = story.variablesState;
        return obj_orks_beaten > 0 && !persist_sharptoof_dislikes_betrayal;
    },
},{
    name: 'Chompa',
    path: 'Chompa.start',
    role: 'The Barman',
    order: 1,
    img: chompaPng,
},
{
    name: 'Krugg',
    path: 'Krugg.start',
    role: 'The Mek',
    order: 5,
    img: kruggPng
},
{
    name: 'Wendy',
    path: 'Wendy.start',
    role: 'The Hunter',
    order: 4,
    img: wendyPng,
    images: {
        hunting: wendy2Png
    }
},
{
    name: 'Skullkrusha',
    path: 'Skullkrusha.start',
    role: 'The Boss',
    order: 3,
    img: skullkrushaPng,
    unlock: (story) => {
        const { obj_orks_beaten, obj_skullkrusha_gone, has_armour } = story.variablesState;
        return !obj_skullkrusha_gone && (obj_orks_beaten > 1 || has_armour)
    }
}];

// TODO can I generate this from the story? it's kinda tough
const profileMap = {
    // chompa
    drinking: "Drinkin'",
    drinking2: "Drinkin'",
    drinking3: "DRINKIN'",
    sobriety: "Soberin' up",
    fancy_stuff: "Fancy Stuff",
    inadequacy: "Feelings of inadequacy",

    // krugg
    // all default

    // wendy
    hunting: "Huntin'",
    eating: "Eatin'",
    weird: "Weirdboyz",
    name: "Gettin' teased 'bout his name",
    softness: "Soft gitz",

    // sharptoof
    cooking: "Cookin'",
    mushrooms: 'Shrooms',
};

function propercaseName(name) {
    return `${name[0].toUpperCase()}${name.substr(1)}`;
}

function extractKey(name, value) {
    if (value) {
        return profileMap[name] || propercaseName(name);
    }
    return '*****';
}

export function getOrk(name) {
    return orks.find((ork) => ork.name === name)
}

export function getCurrentOrk(path = '') {
    const [name] = path.split('.');
    return getOrk(name);
}

export function disableOrk(story, name) {
    const ork = getOrk(name);
    if (ork.unlock) {
        return !ork.unlock(story)
    }
}

export function extractProfile(story, name) {
    if (!story) {
        return;
    }
    const likesKey = `persist_${name}_likes_`;
    const dislikesKey = `persist_${name}_dislikes_`;

    let status;
    try {
        status = story.EvaluateFunction(`status_${name}`);
    } catch(e) { 
        status = "You see an ork."
    }

    const { role } = getOrk(propercaseName(name));

    const result = {
        showName: story.variablesState[`obj_met_${name}`],
        name: propercaseName(name),
        role,
        status,
        likes: {},
        dislikes: {},
        icon: () => {
            if (story.variablesState[`${name}_done`]) return 'heart'
            if (story.variablesState[`${name}_rejected`]) return 'heart-broken'
            if (story.variablesState[`obj_${name}_dead`]) return 'skull'
        },
        isDead: () => {
            return story.variablesState[`obj_${name}_dead`];
        }
    };
    // iterate over all variables
    const vars = story.variablesState._globalVariables;
    for(const [key, obj] of vars) {

        if (key.startsWith(likesKey)) {
            const [prefix, v] = key.split(likesKey);
            result.likes[v] = extractKey(v, obj.value);
        }
        else if (key.startsWith(dislikesKey)) {
            const [prefix, v] = key.split(dislikesKey);
            result.dislikes[v] = extractKey(v, obj.value);
        }
    }

    return result;
};
