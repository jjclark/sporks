import React from 'react';
import ReactDOM from 'react-dom';
import FontFaceObserver from 'fontfaceobserver';

import "core-js/stable";
import "regenerator-runtime/runtime";

import './style.css'

import App from './ui/App';

Promise.all([
    new FontFaceObserver('Carter One').load(),
    new FontFaceObserver('Montserrat').load(),
]).then(startup).catch((e) => {
    console.log("Failed to load fonts")
    console.log(e);
    startup();
});

function startup() {
    ReactDOM.render(React.createElement(App), document.getElementById('react-root'))
}
