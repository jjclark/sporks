// A headless component for playing audio
import { getFile } from '../preload';

const VOLUME = 0.7;
const FADE_DELAY_MS = 2000;

export const FOREGROUND = 1;
export const BACKGROUND = 2;

export const CHOICE = 'choice';
export const CHOICE_TAG = 'choice_with_tag';
export const STORY_COMPLETE = 'story_complete';
export const TROPHY = 'trophy';
export const GALLERY_IN = 'gallery_in';
export const GALLERY_OUT = 'gallery_out';
export const GALLERY_HOVER = 'gallery_hover';

// List of all the tracks available
const tracks = {
    GALLERY: 'gallery',
    CHOMPA: 'chompa',
}

// Metadata for the samples
const samples = {
    gallery: 'intro.mp3',
    chompa: 'chompa.mp3',
    wendy: 'wendy.mp3',
    krugg: 'krugg.mp3',
    skullkrusha: 'skullkrusha.mp3',
    sharptoof: 'sharptoof.mp3',

    death: 'death.mp3',
    'ending-good': 'gameover-good.mp3',
    'ending-bad': 'gameover-bad.mp3',

    drumSingle: 'drum1-single.mp3',
    drumDouble: 'drum1-double.mp3',
    drumTreble: 'drum2-roll.mp3',
    drumroll: 'drumroll.mp3',
    rattle: 'rattle.mp3',
    chime: 'chime.mp3',
    chime2: 'chime2.mp3',
    chimeLow: 'chime-low.mp3',
};

samples[CHOICE] = samples.drumSingle;
samples[CHOICE_TAG] = samples.drumDouble;
samples[STORY_COMPLETE] = samples.drumTreble;
samples[TROPHY] = samples.rattle;
samples[GALLERY_IN] = samples.drumroll;
samples[GALLERY_OUT] = samples.chime2;
samples[GALLERY_HOVER] = samples.chimeLow;

export default function Jukebox({ initialEnabled }) {
    let currentTrack;
    let currentSource;
    let enabled = initialEnabled;
    let isPlaying = true;

    const AudioContext = window.AudioContext || window.webkitAudioContext;
    const backgroundCtx = new AudioContext();
    const foregroundCtx = new AudioContext();

    // Force the volume down a bit
    const gainBg = backgroundCtx.createGain();
    gainBg.gain.setValueAtTime(VOLUME, 0);
    gainBg.connect(backgroundCtx.destination);

    const gainFg = foregroundCtx.createGain();
    gainFg.connect(foregroundCtx.destination);

    async function loadSample(name, track = BACKGROUND, loop) {
        if (samples[name]) {
            const path = `audio/${samples[name]}`;
            const ctx = track === FOREGROUND ? foregroundCtx : backgroundCtx;
            // Cache the decoded buffers
            if (!ctx._cache || !ctx._cache[name]) {
                const arrayBuffer = await getFile(path, ctx);
                const audioBuffer = await ctx.decodeAudioData(arrayBuffer.slice(0));
                if (!ctx._cache) {
                    ctx._cache = {};
                }
                ctx._cache[name] = audioBuffer;
            }
            // Create a new buffer source every time we play the sound
            const sampleSource = ctx.createBufferSource();
            if (track === BACKGROUND) {
                sampleSource.connect(gainBg);
            } else {
                sampleSource.connect(gainFg);
            }
            sampleSource.buffer = ctx._cache[name];
            sampleSource.loop = loop;
            return sampleSource;
        } else {
            console.warn(`WARNING: Couldn't find sample ${name}`);
        }
    }

    const play = async (force) => {
        if (enabled && (force || !isPlaying)) {
            isPlaying = true;
            if (currentSource) {
                await fadeVolume()
            }
        
            if (currentTrack) {
                loadSample(currentTrack, BACKGROUND, true).then((source) => {
                    currentSource = source;
                    //console.log(' playing track ', currentTrack)
                    if (source) {
                        gainBg.gain.linearRampToValueAtTime(VOLUME, backgroundCtx.currentTime + 0.1)
                        source.start();
                    }
                });
            }
        }
    };

    const fadeVolume = () => {
        return new Promise((resolve) => {
            gainBg.gain.linearRampToValueAtTime(0.02, backgroundCtx.currentTime + (FADE_DELAY_MS / 1000))
            setTimeout(() => {
                if (currentSource) {
                    currentSource.stop();
                }
                currentSource = null;
                resolve()
            }, FADE_DELAY_MS)
        });
    }

    const playOnce = (name, track = BACKGROUND) => {
        if (enabled && samples[name]) {
            loadSample(name, track).then((source) => {
                if (source) {
                    source.start();
                }
            });
        }
    }

    const pause = (fade) => {
        return new Promise((resolve) => {
            isPlaying = false;
            if (currentSource) {
                if (fade) {
                    return fadeVolume().then(resolve);
                }
                else {
                    currentSource.stop();
                }
            }
            resolve();
        });
    };

    const setEnabled = (enable) => {
        if (enable && !enabled) {
            enabled = true;
            play();
        }
        else if (!enable && enabled) {
            enabled = false;
            pause();
        }
    };

    const setTrack = (name) => {
        if (name !== currentTrack) {
            currentTrack = name;
            if (enabled) {
                play(true);
            }
        }
    };

    return {
        tracks,
        setEnabled,
        setTrack,
        play, // TODO loop? playtrack?
        playOnce,
        pause,
    }

}