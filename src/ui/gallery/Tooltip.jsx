import React, { useContext } from 'react';

import StoryContext from '../../StoryContext';
import { extractProfile } from '../../story/orks';

const Tooltip = ({ name, target, disabled, onVisit, onCancel }) => {
    const story = useContext(StoryContext);
  
    const profile = extractProfile(story, name.toLowerCase());

    if (profile.isDead()) {
        return (
            <div className={`tooltip profile `}>
                <div className="field name">
                    <img className="title-text" src={`img/ui/${profile.showName ? name.toLowerCase() : 'unknown'}.png`} />
                    <div className={`field icon skull`} />
                </div>
                <div className="field status">{profile.status}</div>
                <div className="panel">
                    <div className="button secondary" onPointerDown={onCancel}>Close</div>
                    <div className="button primary" onPointerDown={onVisit}>Chat</div>
                </div>
            </div>
        )
    }
    if (disabled) {
        return (
            <div className={`tooltip profile disabled `}>
                <div className="field">This ork isn't available right now</div>
                <div className="field">Try speaking to the other orks first</div>
                <div className="panel">
                    <div className="button secondary" onPointerDown={onCancel}>Close</div>
                </div>
            </div>
        )
    }

    const icon = profile.icon();
    return <div className={`tooltip profile`}>
        <div className="field name">
            <img className="title-text" src={`img/ui/${profile.showName ? name.toLowerCase() : 'unknown'}.png`} />
            { icon && <div className={`field icon ${icon}`} />}
        </div>
        <div className="field role">
            <span>{profile.role}</span>
        </div>
        <div className="field status">{profile.status}</div>
        <div className="panel">
            <div className="button secondary" onPointerDown={onCancel}>Close</div>
            <div className="button primary" onPointerDown={onVisit}>Chat</div>
        </div>
    </div>
}

export default Tooltip;