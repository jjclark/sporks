import React, { useEffect, useState } from 'react';
import { CSSTransition } from 'react-transition-group';

import GalleryItem from './GalleryItem';
import { orks } from '../../story/orks';

import { FOREGROUND, GALLERY_IN, GALLERY_OUT } from '../../audio/jukebox';

import './Gallery.css';

const FADE_DEFER = 100;
const FADE_DURATION = 500;

const orderedOrks = orks.sort((a, b) => {
    if (a.order > b.order) return 1;
    if (a.order < b.order) return -1;
});


function Delay({ children, duration }) {
    const [show, setShow] = useState(false);
    useEffect(() => {
        setTimeout(() => {
            setShow(true);
        }, duration)
    });

    return show ? children : <div className="ork"><img style={{visibility:'hidden'}}></img></div>;
}

const fn = () => {};

const Gallery = ({ jukebox, onMenuSelectStart = fn, onMenuSelectComplete = fn }) => {
    const [selected, setSelected] = useState(); // the currently selected
    const [vistTarget, setVisitTarget] = useState(); // the ork we go and see
    const [visible, setVisible] = useState(() => {
        const v = {};
        orks.forEach((o) => v[o.name] = true);
        return v;
    });

    const fadeLastItem = () => {
        setVisible({
            [vistTarget]: false
        });
        setTimeout(() => {
            // Clear the 'global' hover. Not nice.
            document.getElementById('react-root').classList.toggle('hover-active', false);
            // Pass the result back through
            onMenuSelectComplete(vistTarget);
        }, FADE_DURATION);
    };

    useEffect(() => {
        jukebox.playOnce(GALLERY_IN, FOREGROUND);
    }, []);

    useEffect(() => {
        if (vistTarget) {
            // Hide the next ork
            for (const k in visible) {
                if (!vistTarget.startsWith(k) && visible[k]) {
                    setTimeout(() => {
                        setVisible({
                            ...visible,
                            [k]: false
                        })
                    }, FADE_DEFER);
                    return;
                }
            }
            setTimeout(() => {
                // Pass the result back through
                fadeLastItem();
            }, FADE_DURATION / 2);
        }

    }, [vistTarget, visible]);

    const onVisit = (path) => {
        jukebox.playOnce(GALLERY_OUT, FOREGROUND);
        onMenuSelectStart(path);
        setVisitTarget(path);
        
    };

    const onSelect = (name) => {
        setSelected(name)
    };

    let time = 0;
    return (<div className="menu">
        {orderedOrks.map(ork => 
            <Delay duration={time += FADE_DEFER} key={ork.name}>
                <CSSTransition
                    in={visible[ork.name]}
                    appear={visible[ork.name]}
                    timeout={FADE_DURATION}
                    classNames={vistTarget && vistTarget.startsWith(ork.name) ? 'menu-ork-selected' : 'menu-ork'}
                >
                    <GalleryItem
                        jukebox={jukebox}
                        onVisit={onVisit}
                        onSelect={onSelect}
                        defer={FADE_DURATION}
                        deactivate={selected && selected !== ork.name}
                        selected={selected === ork.name}
                        {...ork}
                    />
                </CSSTransition>
            </Delay>
        )}
    </div>);
}


export default Gallery;