import React, { useState, useEffect, useContext, useRef } from 'react';

import Tooltip from './Tooltip';
import StoryContext from '../../StoryContext';
import { disableOrk, extractProfile } from '../../story/orks';
import { FOREGROUND, GALLERY_HOVER } from '../../audio/jukebox';

// This is needlessly confusing
// - deactivated just means the ork isn't interactive, it's backgrounded
// - disabled means the ork can't be visited
const GalleryItem = ({ name, path, onSelect, onVisit, selected, defer, deactivate, jukebox }) => {
    const story = useContext(StoryContext);
    const [hover, setHover] = useState(false);
    // while inactive, we don't respond to mouse events
    const [active, setActive] = useState(false);

    const profile = extractProfile(story, name.toLowerCase());

    let disabled = disableOrk(story, name)

    function toggleMask(v) {
        if (active && !deactivate) {
            // pretty cheeky side-effect, but I need a good way to darken the other orks while
            document.getElementById('react-root').classList.toggle('hover-active', v);
        }
    }

    useEffect(() => {
        setTimeout(() => {
            setActive(true);
        }, defer)
    }, [])
    const ref = useRef();

    const grow = () => {
        ref.current.classList.toggle('grow', true);
        setTimeout(shrink, 150);
    }
    const shrink = () => {
        ref.current.classList.toggle('grow', false);
        ref.current.classList.toggle('shrink', true);
        setTimeout(() => {
            ref.current.classList.toggle('shrink', false);
        }, 69);
    }

    const handleClick = () => {
        if (!deactivate) {
            if (!selected) {
                jukebox.playOnce(GALLERY_HOVER, FOREGROUND);
            }
            toggleMask(true)        
            onSelect(name);
        }
    }

    const handleCancel = () => {
        toggleMask(false);
        onSelect();
    }

    const handleVisit = () => {
        toggleMask(false);
        setTimeout(() => {
            // Needs to be in a timeout or we're fighting over the class name
            // Not a very nice solution really
            grow();
        }, 1);
        onVisit(path)
    }
    const enableOutline = !deactivate && !disabled && (selected || hover)
    const imgSrc = `img/scaled/${name.toLowerCase()}-350x${enableOutline ? '-outline' : ''}.png`;

    const classList = ['ork', selected && 'selected', name.toLowerCase(), profile.isDead() && 'dead'];
    return (<div
        className={classList.filter(Boolean).join(' ')}
        ref={ref}
        key={name}
        >
        <div
            className="img-wrapper"
            onPointerMove={() => setHover(true)}
            onPointerLeave={() => setHover(false)}
        >
            <img
                className={[selected && 'selected', disabled && 'disabled', hover && 'focus'].filter(c => c).join(' ')}
                src={imgSrc}
                onPointerDown={handleClick}>
            </img>
        </div>
        {
            selected && <Tooltip name={name} target={ref.current} disabled={disabled} onCancel={handleCancel} onVisit={handleVisit}/>
        }
    </div>);
}

export default GalleryItem;