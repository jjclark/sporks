import React, { useState } from 'react';
import { CSSTransition } from 'react-transition-group';

import './Modal.css';

// Animation for this is really hard
// We need to only callback dismiss after the hide animation has finished
const Modal = ({ title, children, dismiss }) => {
    // TODO probbaly need to renaming this to closing/isClosing and make it a bool
    const [visible, setVisible] = useState(null);
    
    const close= () => {
        setVisible(false)
    }
    
    const afterHide = () => {
        setVisible(null);
        dismiss();
    }

    let hide = visible === false || !children;

    return (
        <div className={['modal', !children && 'hide'].filter(Boolean).join(' ')}>
            <CSSTransition
                in={!hide}
                appear={!hide}
                timeout={300}
                classNames={'modal-inner'}
                onExited={afterHide}
            >
                <div className="modal-inner">
                    <div className="modal-header">
                        <h1>{title}</h1>
                    </div>
                    <div className="modal-content">
                        {children}
                    </div>
                    <div className="modal-footer" onPointerDown={close}>
                        <span>CLOSE</span>
                    </div>
                </div>
            </CSSTransition>
        </div>
    );
};

export default Modal;