import React, { useContext } from 'react';

import { Trophies } from '../screens/Trophies';
import { Inventory } from '../screens/Inventory';
import Credits from '../screens/Credits';
import pkg from '../../../package.json';
import { SettingsContext } from '../../SettingsContext';

import './Footer.css';

const { version } = pkg;

const Stuck = ({ reset }) => (<div style={{ textAlign: 'center'}}>
    <p>
        Stuck? Messed up? Want to start over?
    </p>
    <p>This will reset the game back to the beginning.</p>
    <p>Your Stories progress <b>will</b> be saved.</p>
    <a onPointerDown={reset}>Start Over</a>
</div>);

export default function Footer({ openModal, reset }) {
    const { context, setOptions } = useContext(SettingsContext);

    return <div className="footer">
        <div className="col">
            <span className="button" onPointerDown={() => openModal({
                title: 'Stories',
                content: <Trophies />
            })}>Stories</span>
            <span className="button" onPointerDown={() => openModal({
                title: 'Inventory',
                content: <Inventory />
            })}>Inventory</span>
            <span className="button" onPointerDown={() => openModal({
                title: 'Credits',
                content: <Credits />
            })}>Credits</span>
            <span className="button" onPointerDown={() => setOptions({ sound: !context.options.sound })} >
                Sound { context.options.sound ? 'ON' : 'OFF'}
            </span>
            <span className="button" onPointerDown={() => openModal({
                title: 'Restart',
                content: <Stuck reset={reset} />,
            })}>Restart</span>
        </div>
        <div className="col">
            <span>ORKWARD: The Space Ork Dating Game</span>
            <span>{version}</span>
        </div>
    </div>
}