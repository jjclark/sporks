import React, { useState, useContext, useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { CSSTransition } from 'react-transition-group';

import preload from '../preload';
import { SettingsContext } from '../SettingsContext';
import Intro from './screens/Intro';
import Outro from './screens/Outro';
import Death from './screens/Death';
import Footer from './components/Footer';
import { Trophies, getTrophyData, getTrophyCount, setTrophyCount, TrophyCard, markTrophiesStarted } from './screens/Trophies';
import Gallery from './gallery/Gallery';
import Modal from './components/Modal';
import annotateChoice from './helpers/annotate-choice';
import annotateGuideMargin from './helpers/annotate-guide-margin';
import StoryContext from '../StoryContext';
import InkReact from '../ink/ink-react';
import { getCurrentOrk, extractProfile } from '../story/orks';
import Jukebox, { CHOICE, CHOICE_TAG, STORY_COMPLETE, TROPHY, FOREGROUND } from '../audio/jukebox';
import storyContent from '../../lib/sporks.json';

import 'react-toastify/dist/ReactToastify.css';

const states = {
    GAME: 'game',
    DEATH: 'death',
    GALLERY: 'gallery',
    INTRO: 'intro',
    OUTRO: 'outro',
}

const FrontEnd = ({ initialPath }) => {
    const { context, setVars, setState: setGameState } = useContext(SettingsContext);
    const [state, setState] = useState({
        name: initialPath ? states.GAME : states.INTRO,
        path: initialPath
    });
    const [loading, setLoading] = useState(true);
    const [story, setStory] = useState();
    const [image, setImage] = useState();
    const [modal, setModal] = useState({});
    const [jukebox] = useState(() => new Jukebox({ initialEnabled: context.options.sound }));
    jukebox.setEnabled(context.options.sound);

    useEffect(async () => {
        await preload();
        setLoading(false);
    }, [])

    const handleMenuSelect = (path, name) => {
        // Timeout to avoid a flicker
        setTimeout(() => {
            setState({ name: states.GAME, path })
        }, 100);
    };

    const handleMenuPreselect = (path) => {
        const [name] = path.split('.');
        const ork = name.toLowerCase();
        jukebox.setTrack(ork);
    };

    const handleClickTrophyToast = () => {
        setModalChildren(<Trophies />);
    };

    const handleTag = (tag, value) => {
        switch (tag) {
            case 'DEATH':
                setState({ name: states.DEATH });
                break;
            case 'USE_ITEM':
                setVars({
                    [`persist_used_${value}`]: true
                });
                story.variablesState[`persist_used_${value}`] = true;
                break;
            case 'TROPHY':
                const count = getTrophyCount(story, value);
                if (count <= 1) {
                    console.log(value)
                    // Ensure that the story count is at least 0
                    // (vars initialised to -1 might not be)
                    setTrophyCount(story, value, Math.max(1, count));
                    setVars({
                        [`trophy_${value}`]: 2
                    });
                    setTimeout(() => {
                        jukebox.playOnce(TROPHY, FOREGROUND);
                        toast(<>
                            <span onPointerDown={handleClickTrophyToast}>
                                <h3>Story completed!</h3>
                                <TrophyCard data={getTrophyData(value)} />
                            </span>
                        </>);
                    }, 1500)
                }
                break;
        case 'IMAGE':
            if (value) {
                const activeOrk = getCurrentOrk(state.path);
                setImage(activeOrk.images[value]);
            } else {
                setImage(null);
            }
            break;
        case 'AUDIO':
            jukebox.pause().then(() => {
                jukebox.playOnce(value);
            });
            break;
        case 'ENDING':
            // This isn't very nice, but the first word is the image path, the rest is text
            const [img, ...rest] = value.split(' ');
            setState({ name: states.OUTRO, text: rest.join(' '), img });
            break;
        case 'MENU':
            setImage(null); // always reset the image
            const isDeadEnd = story.EvaluateFunction('dead_end');
            if (isDeadEnd) {
                // if it's a dead end, we do something different
                setTimeout(() => {
                    setState({ name: states.GAME, path: 'exit' });
                }, 100)
                
            } else {
                setTimeout(() => {
                    setState({ name: states.GALLERY, path: '' });
                    jukebox.setTrack('gallery');
                }, 400);
            }
            break;
        default:
            break;
        }
    }

    const handleChoiceTag = (tag, el) => {
        annotateChoice(context, story, tag, el);
    };

    const handleLinkClicked = (parentDom, choiceTags = []) => {
        if (choiceTags.length) {
            if (choiceTags.find(tag => tag.endsWith("/end"))) {
                jukebox.playOnce(STORY_COMPLETE, FOREGROUND)
            } else {
                jukebox.playOnce(CHOICE_TAG, FOREGROUND)
            }
            const wrapper = document.createElement('div');
            wrapper.className="trophy-wrapper";
            parentDom.appendChild(wrapper);
            let delay = -200;
            choiceTags.forEach((tag) => {
                const [name, ...trophies] = tag.split(' ');
                if (name === "GUIDE") {
                    const diff = markTrophiesStarted(story, trophies);
                    if (Object.keys(diff).length) {
                        setGameState(diff);
                    }
                }
                setTimeout(() => {
                    annotateGuideMargin(story, tag, wrapper);
                }, delay += 200)
            });
        } else {
            jukebox.playOnce(CHOICE, FOREGROUND)
        }
    };

    const restart = () => {
        if (story) {
            story.reset();
        }
        setState({ name: states.INTRO });
    };

    let content = <></>;
    switch (state.name) {
        case states.DEATH:
            story.reset(true);
            content = <Death onContinue={() => handleTag('MENU')}/>
            break;
        case states.INTRO:
            content = <Intro loading={loading} jukebox={jukebox} onContinue={() => setState({ name: states.GALLERY })} />;
            break;
        case states.OUTRO:
            content = <Outro text={state.text} img={state.img} onLeave={restart} />;
            break;
        case states.GALLERY:
            content = <Gallery
                jukebox={jukebox}
                onMenuSelectStart={handleMenuPreselect}
                onMenuSelectComplete={handleMenuSelect}
            />
            break;
    }
    const activeOrk = getCurrentOrk(state.path);
    const activeProfile = activeOrk && extractProfile(story, activeOrk.name.toLowerCase());
    let isDead = false;
    if (activeProfile) {
        if (activeProfile.isDead()) {
            isDead = true;
            jukebox.setTrack();
        } else {
            jukebox.setTrack(activeOrk.name.toLowerCase());
        }
    }

    return (
        <StoryContext.Provider value={story}>
            {content}
            <div className="ink-wrapper">
                <CSSTransition
                    in={state.name === states.GAME}
                    appear={state.name === states.GAME}
                    timeout={300}
                    classNames={'ink-react'}
                >
                    <InkReact
                        content={storyContent}
                        onStoryCreated={setStory}
                        onLinkClicked={handleLinkClicked}
                        onTag={handleTag}
                        onChoiceTag={handleChoiceTag}
                        path={state.path}
                        vars={context.vars}
                    />
                </CSSTransition>
                <CSSTransition
                    in={activeOrk}
                    appear={activeOrk}
                    timeout={300}
                    classNames={'ink-img'}
                >
                    <div className={`main-img ${activeOrk && isDead ? 'dead' : ''}`}>
                        <div className="img-border"></div>
                        <div className="img-wrapper">
                            {activeOrk && <img src={image || activeOrk.img} />}
                        </div>
                    </div>
                </CSSTransition>
            </div>
            <Footer openModal={(component) => setModal(component)} reset={() => {
                 setModal({});
                 restart();
            }}/>
            <Modal title={modal.title} dismiss={() => setModal({})}>{modal.content}</Modal>
            <ToastContainer
                position="bottom-right"
                autoClose={3000}
                hideProgressBar={true}
                closeButton={false} />
        </StoryContext.Provider>
    );
}

export default FrontEnd;