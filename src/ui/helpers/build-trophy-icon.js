export default (name, overlay) => {
    const wrapper = document.createElement('div')
    const icon = document.createElement('img')
    const style=[
        `background-image: url(img/trophies/${name}.png)`,
        'width:32px',
        'height:32px',
    ];
    if (overlay) {
        style.push(
            'background-size: 28px',
            'background-position-x: center',
            'background-position-y: top',
            'background-repeat: no-repeat'
        );
        // anchor an actual image in the bottom right
        icon.src=`img/trophies/${overlay}.png`;
    } else {
        style.push('background-size: contain');
        // 1x1 transparent
        icon.src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5QUIDywp99Yz8QAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAAC0lEQVQI12NgAAIAAAUAAeImBZsAAAAASUVORK5CYII="
    }
    icon.style=style.join(';');
    wrapper.className = 'trophy-icon';
    wrapper.appendChild(icon);
    return wrapper;
}
