import buildTrophyIcon from './build-trophy-icon';
import { getTrophyData} from '../screens/Trophies';

const debugMode = false;

function handleGuide(context, trophies, el) {
    const seenGuides = trophies.filter(name => debugMode || context.state[name]);
    if (seenGuides.length == 1) {
        const [t] = seenGuides;
        let text;
        let iconName;
        if (t.match(/^death\//)) {
            text = 'This choice leads to certain death';
            iconName = 'death';
        } else {
            const { title, name } = getTrophyData(t);
            text = `This choice is part of the ${title} story`;
            iconName = name;
        }

        const icon = buildTrophyIcon(iconName);
        icon.title = text;
        el.insertBefore(icon, el.firstChild);
    } else if (seenGuides.length > 1) {
        const icon = buildTrophyIcon('multi');
        const text = 'This choice is part of multiple stories: \n' + seenGuides.map(t => {
            if (t.match(/^death\//)) {
                return '* Certain death'
            }
            const { title } = getTrophyData(t);
            return `* ${title}`;
        }).join('\n');
        icon.title = text;
        el.insertBefore(icon, el.firstChild);
    }
}

function handleRequire(story, [name], el) {
    if ( debugMode || story.variablesState[`persist_used_${name}`]) {
        const icon = buildTrophyIcon(name);
        icon.className += " item";
        icon.title = `This choice requires the ${name} item to succeed`;
        el.append(icon);
    }
}

export default function annotateChoice(context, story, tag, el) {
    try {
        const [name, ...value] = tag.split(' ');
        if (name.startsWith('GUIDE')) {
            handleGuide(context, value, el);
        }
        if (name === 'REQUIRE') {
            handleRequire(story, value, el);
        }
    } catch(e) {
        console.error('Error annotating ', tag, e)
    }
}
