import { getTrophyData} from '../screens/Trophies';
import { getItemName, hasItem } from '../screens/Inventory';
import buildTrophyIcon from './build-trophy-icon';

/*
 Draw trophy icons in the margin to the left of the text
*/
function append(el, parent) {
    el.classList.add("hide");
    parent.appendChild(el);
    setTimeout(function() { el.classList.remove("hide") }, 200);
}

export default function annotateGuideMargin(story, tag = '', dom)  {
    const [name, ...values] = tag.split(' ');
    if (name === 'REQUIRE') {
        if (hasItem(story, values[0])) {
            const icon = buildTrophyIcon(values[0]);
            icon.className += " item";
            icon.title = `You used ${getItemName(values[0])}`;
            append(icon, dom);
        }
    }
    if (name === 'ACQUIRE') {
        const icon = buildTrophyIcon(values[0]);
        icon.className += " item";
        icon.title = `You acquired ${getItemName(values[0])}`;
        append(icon, dom);
    }
    if (name === 'GUIDE' && values.length) {
        values.forEach((t) => {
            const trophy = getTrophyData(t);
            if (trophy && !t.startsWith("death")) {
                const { title, name } = trophy;
                const done = t.endsWith("/end");
                const icon = buildTrophyIcon(name, done ? 'done' : null);
                let state = 'advanced';
                if (done) {
                    state = 'completed';
                } else if (t.match(/\/start(\-.*$)?/)) {
                    state = 'started'
                }
                icon.className+=" update" + (done ? ' completed' : '');
                icon.title = `You have ${state} the ${title} story`;
                append(icon, dom);
            }
        });
    }
}
