import React from 'react';
import queryString from 'query-string';

import FrontEnd from './FrontEnd';

import { SettingsContextProvider } from '../SettingsContext';

const { path: initialPath } = queryString.parse(window.location.search);

function App() {
    return (
        <SettingsContextProvider>
            <FrontEnd initialPath={initialPath} />
        </SettingsContextProvider>
    );
}

export default App;