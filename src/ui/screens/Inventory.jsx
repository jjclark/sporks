// TODO I probably want to make this a react component
// So that I can better take advantage of persistence
// need a useTrophies hook I guess which traps the logic
import React, { useContext } from 'react';
import queryString from 'query-string';

import StoryContext from '../../StoryContext';

import data from '../../story/items.js';

import './Inventory.css';

const { show_items } = queryString.parse(window.location.search);

export const hasItem = (story, name) => story.variablesState[`has_${name}`];

export const getItemName = (id) => {
    const item = data.find((i) => i.id === id);
    if (item) {
        return item.name;
    }
    return "";
}

const getDesc = ({ description }) => {
    if (description.call) {
        return description();
    } else {
        return description;
    }
};

export const ItemCard = ({ data }) => {
    return (
        <div className="item">
            <img src={`img/trophies/${data.icon}.png`}></img>
            <div className="title">{data.name}</div>
            <div className="description">{getDesc(data)}</div>
        </div>
    );
}

export const Inventory = () => {
    const story = useContext(StoryContext);
    const items = data.filter(t => show_items || hasItem(story, t.id));
    if (items.length) {
        if (items.length % 2 > 0) {
            items.push(false);
        }
        return (
            <div className="item-list">{
                items.map((t) => {
                    if (t) return <ItemCard key={t.name} data={t} />
                    return <div className="item"/>;
                })
            }</div>
        );
    }
    return <p style={{ textAlign: 'center'}}>You have no items.</p>
}
