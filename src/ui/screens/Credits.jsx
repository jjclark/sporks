import React from 'react';

import './Credits.css';

const Icon = ({ link, name }) => 
    <a href={link} target="none">
        <span className={`icon ${name}`} />
    </a>;


const Credits = () => {
    return (
        <div className="credits-wrapper">
            <div className="credit">
                <div className="credit-role">Design, Writing, Code</div>
                <div className="credit-name">
                    Joseph J Clark
                    <Icon name="twitter" link="https://www.twitter.com/jjc_uk" />
                </div>
            </div>
            <div className="credit">
                <div className="credit-role">Art</div>
                <div className="credit-name">
                    Isaac Garabito
                    <Icon name="instagram" link="https://www.instagram.com/isaacgarabito" />
                </div>
            </div>
            <div className="credit">
                <div className="credit-role">Music</div>
                <div className="credit-name">
                    Drew Twyman
                    <Icon name="web" link="https://www.drewtwyman.com" />
                </div>
            </div>
            <div className="credit">
                <div className="credit-role">Powered by</div>
                <div className="credit-name">
                    Ink
                    <Icon name="web" link="https://www.inklestudios.com/ink" />
                </div>
            </div>
            <div className="credit">
                <div className="credit-role">Beta Testers</div>
                <div className="credit-list beta">
                    <span>Isilme</span>
                    <span>Kaleigh Scullion</span>
                    <span>Kellie Lu</span>
                    <span>Michael Bowerman</span>
                    <span>MJ</span>
                    <span>paladinrjp</span>
                    <span>PinkTail Studio</span>
                    <span>Tom Betts</span>
                </div>
            </div>
            <div className="credit">
                <div className="credit-role">Technical and Emotional Support</div>
                <div className="credit-list">
                    <span>BicycleName</span>
                    <span>Dunk</span>
                    <span>Fishing Cactus</span>
                    <span>Isilme</span>
                    <span>Pipsissiwa</span>
                    <span>qw3ry</span>
                    <span>Sarah Witzke</span>
                    <span>Selsynn</span>
                    <span>The Mighty Git</span>
                    <span>TheThibz</span>
                    <span>Vectorified</span>
                    <span>VirginRedemption</span>
                </div>
            </div>

        </div>
    );
};

export default Credits;