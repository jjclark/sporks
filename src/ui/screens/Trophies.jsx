// TODO I probably want to make this a react component
// So that I can better take advantage of persistence
// need a useTrophies hook I guess which traps the logic
import React, { useContext } from 'react';
import queryString from 'query-string';

import StoryContext from '../../StoryContext';

import data from '../../story/trophies.js';

import './Trophies.css';

const { show_trophies } = queryString.parse(window.location.search);

// get the namespaced trophyname
// Yeah, not actually a very nice pattern
function tname(name) {
    return `trophy_${name}`;
}

export const isTrophyStarted = (story, name) => {
    console.log(story.variablesState[tname(name)])
    story.variablesState[tname(name)] >= 0;
}

export const markTrophiesStarted = (story, names = []) => {
    const state = {}; // written back to persistent game state
    names.forEach((name) => {
        const key = tname(name);
        const val = story.variablesState[key];
        if (val == null || val < 0) {
            state[name] = true;
            // if (story.variablesState.hasOwnProperty(key)) {
            //     story.variablesState[key] = 0;
            // } else {
            //     console.log("Didn't write key ", key)
            // }
        }
    });
    return state;
}

export const getTrophyCount = (story, name) => {
    return story.variablesState[tname(name)];
}

export const setTrophyCount = (story, name, count) => {
    story.variablesState[tname(name)] = count;
}

export function getTrophyData(name) {
    // Check if a path is namespaced, ie, death/manners.
    // If so, use the path root for the icon and ignore the rest
    const path = name.match(/(\w+)\/\w+/);
    if (path && path[1]) {
        name = path[1];
    }

    return data.find((t) => t.name === name);
}

export const TrophyCard = ({ data, done = true }) => {
    if (!data) {
        console.error('Trophy data not found')
        return null;
    }
    return (
        <div className={['trophy', !done && 'locked'].filter(Boolean).join(' ')}>
            <img src={done ? `img/trophies/${data.name}.png` : 'img/trophies/locked.png'}></img>
            <div className="title">{data.title}</div>
            <div className="description">{done ? data.description : 'INCOMPLETE'}</div>
        </div>
    );

}

const buildGroups = () => {
    const groups = {};
    data.forEach((d) => {
        const name = d.group || 'And da rest'
        if (!groups[name]) {
            groups[name] = [];
        }
        groups[name].push(d);
    });
    return groups;
}

export const Trophies = () => {
    const story = useContext(StoryContext);
    const groups =  buildGroups();
    return (
        <div className="trophy-list">{
            Object.entries(groups).map(([name, group]) => {
                const items = group.map((t) => {
                    const done = show_trophies || getTrophyCount(story, t.name) > 0;
                    return <TrophyCard key={t.name} data={t} done={done} />
                });
                // Hack around alignment because I can't work out the CSS
                if((group.length % 2) != 0) {
                    items.push(<div className="trophy"/>)
                }
                return (<div className="trophy-group">
                    <h2>{name}</h2>
                    <div>{items}</div>
                </div>);
            })
        }</div>
    );
}
