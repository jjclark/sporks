import React, { useState } from 'react';
import { CHOICE, FOREGROUND } from '../../audio/jukebox';
import './Intro.css';

const pages = [
    <>
        <div className="header">
            <img id="title-img" src="img/ui/orkward-title-180x.png" alt="ORKWARD title text"></img>
            <h2>The Space Ork Dating Game</h2>
        </div>
    </>,
    <>
        <p>In the grim darkness of the future, they say, there is only war.</p>
        <p>You are tired of conflict. You are tired of pain.</p>
        <p>You stand in the center of a raucous space ork camp, and wonder if maybe you can find something more.</p>
    </>,
    <>
        <p>Chaos.</p>
        <p>Orks in their dozens stagger, clank, rage and stomp between the crude metal structures.</p>
        <p>The air is thick with shouting, gunfire and smoke.</p>
        <p>You identify a handful of orks who look capable of conversation...</p>
    </>,
];

const Intro = ({ loading, jukebox, onContinue }) => {
    const [page, setPage] = useState(0);
    const isFirst = page === 0;
    const isLast = page === pages.length - 1;
    const next = () => {
        jukebox.playOnce(CHOICE, FOREGROUND)
        if (page === 0) {
            jukebox.setTrack('gallery');
        }
        if (isLast) {
            onContinue();
        } else {
            setPage(page + 1);
        }
    };

    return (
        <div className="container intro">
            {pages[page]}
            {loading ? 
                // TODO some kind of spinner
                <p>LOADING</p>
                :
                <p className="choice">
                    <a onPointerDown={next}>{ isFirst ? 'Click to begin' : 'Next'}</a>
                </p>
            }
        </div>
   );
};

export default Intro;