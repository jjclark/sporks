import React, { useState } from 'react';
import './Death.css';

const Death = ({ onContinue }) => {
    const [fade, setFade] = useState(false);

    const startFade = () => {
        setFade(true)
        setTimeout(() => {
            onContinue();
            setTimeout(() => {
                setFade(false);
            }, 500)
        }, 1400)
    }

    return  (
        <div className={`death-wrapper ${fade ?'fade-out' : ''}`}>
            <div className="container">
            <p>The world spins on its axis, oblivious and uncaring.</p>
            <p>A light shines anew.</p>
            <p className="choice">
                <a onPointerDown={startFade}>Continue</a>
            </p>
            </div>
        </div>
    );
}

export default Death;