import React, { useState } from 'react';

import Credits from './Credits'
import './Outro.css';

const Outro = ({ text, img, onLeave }) => {
    const [page, setPage] = useState(1);
    let content;

    if (page == 1) {
        content = <>
            <p>{text}</p>
            <p className="choice">
                <a onPointerDown={() => setPage(2)}>Next</a>
            </p>
        </>;
    } else  {
        content = <>
            <h2 style={{ marginTop: '1em' }}>Thanks for playing!</h2>
            <p className="choice">
                <a onPointerDown={onLeave}>Play Again</a>
            </p>
            <Credits />
            <p className="choice">
                <a onPointerDown={onLeave}>Play Again</a>
            </p>
        </>
    }

    return (<>
        <div className="outro-img" style={{ backgroundImage: `url(img/endings/${img})` }}/>
        <div className={`container outro page${page}`}>
            {content} 
        </div>
    </>)
}

export default Outro;