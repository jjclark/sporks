const cache = {};

const images = [
    "img/scaled/chompa-350x.png",
    "img/scaled/wendy-350x.png",
    "img/scaled/krugg-350x.png",
    "img/scaled/skullkrusha-350x.png",
    "img/scaled/sharptoof-350x.png"
];

const audio = [
    "audio/intro.mp3",
    "audio/drum1-single.mp3",
    "audio/drum2-roll.mp3",
    "audio/drumroll.mp3",
    "audio/chime2.mp3",
    "audio/rattle.mp3",
    "audio/chime-low.mp3",
    "audio/chompa.mp3",
    "audio/wendy.mp3",
    "audio/krugg.mp3",
    "audio/sharptoof.mp3",
    "audio/skullkrusha.mp3",
    "audio/death.mp3",
    "audio/gameover-good.mp3",
    "audio/gameover-bad.mp3",
]

const loadImage = (path) => {
    return new Promise((resolve) => {
        const img=new Image();
        img.src=path;
        img.onload = () => resolve();
    });
};

const loadAudio = async (path) => {
    const response = await fetch(path);
    cache[path] = await response.arrayBuffer();
    return cache[path];
}

export const preload = () => {
    return Promise.all([
        ...images.map((i) => loadImage(i)),
        ...audio.map((i) => loadAudio(i)),
    ]);
}

export const getFile = async (path) => {
    if (cache[path]) {
        return cache[path];
    }
    // in case it wasn't preloaded, load it now
    if (path.endsWith('.mp3')) {
        return loadAudio(path);
    }

    // TODO other stuff?

}

export default preload;