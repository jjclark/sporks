// Persistent context for settings and story state
import React, { useState, useCallback }  from 'react'

const STORAGE_KEY = 'orkward.state'
const STORAGE_VERSION = 1;
const debug = false;
const saveVars = true;

const path = `${STORAGE_KEY}.${STORAGE_VERSION}`

let defaultContext;
try {
    // TODO handle migrations for prior versions
    defaultContext = JSON.parse(localStorage.getItem(path));
    if (!defaultContext.options || !defaultContext.vars) throw new Error('Failed to read saved state');
    if (debug) console.log('Restored app state from cache')
} catch(e) {
    if (debug) console.log('Loading default app state')
    defaultContext = {
        options: {
            sound: true,
        },
        vars: {}, // ink vars
        // misc game state
        state: {
            // TODO would rather this was driven by ink
            'death/skullkrusha_fake': true
        }
    };
}


function saveLocally(obj) {
    if(debug) console.log('saving state', obj)
    localStorage.setItem(path, JSON.stringify(obj));
}

export const SettingsContext = React.createContext({
    context: defaultContext,
    setOptions: () => {},
    setVars: () => {},
    setState: () => {},
});

// TODO this is so messy
export const SettingsContextProvider = ({ children }) => {
    const [context, setContext] = useState(defaultContext);
    
    const updateAndSave = (obj) => {
        setContext(obj);
        saveLocally(obj);
    };

    const setOptions = useCallback((obj) => {
        updateAndSave({
            ...context,
            options: {
                ...context.options,
                ...obj
            }
        });    
    }, [context]);

    const setVars = useCallback((obj) => {
        const newContext = {
            ...context,
            vars: {
                ...context.vars,
                ...obj
            }
        };
        if (saveVars) {
            updateAndSave(newContext);
        } else {
            setContext(newContext);
        }
    }, [context, setContext]);

    
    const setState = useCallback((obj) => {
        const newContext = {
            ...context,
            state: {
                ...context.state,
                ...obj
            }
        };
        if (saveVars) {
            updateAndSave(newContext);
        } else {
            setContext(newContext);
        }
    }, [context, setContext]);

    return (
      <SettingsContext.Provider value={{ context, setOptions, setVars, setState }}>
        {children}
      </SettingsContext.Provider>
    );
}