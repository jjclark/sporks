/*
    This is a modified version of of the web player shipped with inky
    
    It bootstraps a renderer inside a React component
*/
import React, { useEffect } from 'react';

import './ink.css';

const noop = () => {};

function InkReact({
        content,
        path,
        title: initialTitle,
        byline: initialByline,
        vars,
        onTag = noop,
        onChoiceTag = noop,
        onStoryCreated = noop,
        onLinkClicked = noop,
    }) {
    const storyContainer = React.useRef();
    const outerScrollContainer = React.useRef();
    
    const [story, setStory] = React.useState();

    // Because react isn't controlling the DOM, we're writing stale functions
    // to ink's click handlers
    // Use the callbacks ref to get over this.
    const callbacks = React.useRef({});
    Object.assign(callbacks.current, {
        onTag,
        onChoiceTag,
        onStoryCreated,
        onLinkClicked
    });

    // TODO not used right now
    const [byline, setByLine] = React.useState(initialByline);
    const [title, setTitle] = React.useState(initialTitle);

    // Initialise the story
    useEffect(() => {
        if (story) {
            // hotload
            removeAll();
        }
        const s =  new inkjs.Story(content);
        // set any vars
        if (vars) {
            Object.entries(vars).forEach(([key, value]) => {
                try {
                    s.variablesState[key] = value;
                } catch(e) {
                    console.warn(`Failed to set ink var ${key}:`);
                    console.warn(e.message)
                }
            });
        }
        callbacks.current.onStoryCreated(s);
        setStory(s);
    }, [content]);

    useEffect(() => {
        if (story) {
            window.story = story;
            story.reset = restart;
            mount()

            story.ChoosePathString(path);
            continueStory()
        }
    }, [story])

    useEffect(() => {
        if (story) {
            clear()
            story.ChoosePathString(path);
            continueStory()
       }
    }, [path])

    return (
        <div ref={outerScrollContainer} className="outerContainer">
            <div ref={storyContainer} className="ink container"></div>
        </div>
    );

    function clear() {
        removeAll();
    }

    function encounterCustomTag(tag, value) {
        if (callbacks.current.onTag) {
            callbacks.current.onTag(tag, value);
        }
    }

    function mount() {
        // Global tags - those at the top of the ink file
        // We support:
        //  # theme: dark
        //  # author: Your Name
        //  # title: story title
        // TODO - this isn't really supported now because the wrapper handles it
        var globalTags = story.globalTags;
        if( globalTags ) {
            for(var i=0; i<story.globalTags.length; i++) {
                var globalTag = story.globalTags[i];
                var splitTag = splitPropertyTag(globalTag);
                
                // THEME: dark
                if( splitTag && splitTag.property == "theme" ) {
                    document.body.classList.add(splitTag.val);
                }
                
                // author: Your Name
                else if( splitTag && splitTag.property == "author" ) {
                    setByLine("by "+splitTag.val)
                }

                // title: story title
                else if( splitTag && splitTag.property == "title" ) {
                    setTitle(splitTag.val)
                }
            }
        }

        // TODO JC I don't want to start straightaway, I want my story to do it
        // Need a better way to drive this really
        // Kick off the start of the story!
        // continueStory(true);
    }

    // Main story processing function. Each time this is called it generates
    // all the next content up as far as the next set of choices.
    function continueStory(firstTime) {

        var paragraphIndex = 0;
        var delay = 0.0;
        
        // Don't over-scroll past new content
        var previousBottomEdge = firstTime ? 0 : contentBottomEdgeY();

        // remove the 'new' class from the last p elements
        // Walk back through the tree untl we find a non-new p or we run out of siblings
        let last = storyContainer.current.lastChild;
        while (last) {
            if (last.tagName.toLowerCase() === "p") {
                if (last.classList.contains('new')) {
                    last.classList.remove('new');
                } else {
                    break;
                }
            }
            last = last.previousElementSibling;
        }

        let abort = false;
        // Generate story text - loop through available content
        while(story.canContinue) {
            // Get ink to generate the next paragraph
            var paragraphText = story.Continue();
            var tags = story.currentTags;
            
            // Any special tags included with this line
            var customClasses = ['new'];
            for(var i=0; i<tags.length; i++) {
                var tag = tags[i];

                // Detect tags of the form "X: Y". Currently used for IMAGE and CLASS but could be
                // customised to be used for other things too.
                var splitTag = splitPropertyTag(tag);

                // // IMAGE: src
                // if( splitTag && splitTag.property == "IMAGE" ) {
                //     var imageElement = document.createElement('img');
                //     imageElement.src = splitTag.val;
                //     storyContainer.current.appendChild(imageElement);

                //     showAfter(delay, imageElement);
                //     delay += 200.0;
                // }

                // CLASS: className
                if( splitTag && splitTag.property == "CLASS" ) {
                    customClasses.push(splitTag.val);
                }

                // TODO remember this is customised code
                // CLEAR - removes all existing content
                // RESTART - clears everything and restarts the story from the beginning
                else if( tag == "CLEAR" || tag == "RESTART" ) {
                    // Comment out this line if you want to leave the header visible when clearing
                    setVisible(".header", false);

                    if (tag === "CLEAR") {
                        clear()
                    }

                    if( tag == "RESTART" ) {
                        clear();
                        restart();
                        abort = true;
                    }
                }

                if (splitTag.property) {
                    encounterCustomTag(splitTag.property, splitTag.val)
                } else {
                    encounterCustomTag(tag)
                }
            }

            if (!abort) {
                // Create paragraph element (initially hidden)
                var paragraphElement = document.createElement('p');
                paragraphElement.innerHTML = paragraphText;
                storyContainer.current.appendChild(paragraphElement);
                
                // Add any custom classes derived from ink tags
                for(var i=0; i<customClasses.length; i++)
                    paragraphElement.classList.add(customClasses[i]);

                // Fade in paragraph after a short delay
                showAfter(delay, paragraphElement);
                delay += 200;
            }
        }

        // Create HTML choices from ink choices
        story.currentChoices.forEach(function(choice) {
            // Choice tag extraction from elliotherriman
            // https://gist.github.com/elliotherriman/357bd2a063b102834ec29846c94199e0
            // array of all the content following a given choice
            var choiceContent = story.PointerAtPath(choice.targetPath).container._content;
            var choiceTags = [];

            for (var i = 0; i < choiceContent.length; i++)
            {
                // only tags seem to have this property
                if (choiceContent[i].text != null)
                {
                    choiceTags.push(choiceContent[i].text);
                }
                // if reaches string that isn't glued, or if reaches linebreak, then tag wasn't intended as part of the choice
                else if (typeof choiceContent[i].value === "string")
                {
                    if (choiceContent[i]._isNewline) break;
                }
            };

            // Create paragraph with anchor element
            var choiceParagraphElement = document.createElement('p');
            choiceParagraphElement.classList.add("choice");
            choiceParagraphElement.innerHTML = `<a href='#'>${choice.text}</a>`
            storyContainer.current.appendChild(choiceParagraphElement);

            choiceTags.forEach((tag) => {
                callbacks.current.onChoiceTag(tag, choiceParagraphElement);
            });

            // Fade choice in after a short delay
            showAfter(delay, choiceParagraphElement);
            delay += 200.0;

            // Click on choice
            var choiceAnchorEl = choiceParagraphElement.querySelectorAll("a")[0];
            choiceAnchorEl.addEventListener("click", function(event) {

                // Don't follow <a> link
                event.preventDefault();

                // Remove all existing choices
                removeAll("p.choice");

                // Add a line break between distinct sections
                // TODO option drive this
                storyContainer.current.appendChild(document.createElement('hr'));

                callbacks.current.onLinkClicked(storyContainer.current, choiceTags);

                // Tell the story where to go next
                story.ChooseChoiceIndex(choice.index);

                // Aaand loop
                continueStory();
            });
        });

        // Extend height to fit
        // We do this manually so that removing elements and creating new ones doesn't
        // cause the height (and therefore scroll) to jump backwards temporarily.
        // storyContainer.current.style.height = contentBottomEdgeY()+"px";

        if( !firstTime )
            scrollDown(previousBottomEdge);
    }

    function restart(soft) {
		// CUSTOM
		// Re-apply any persist state after resetting
		const restore = new Map();
		for (const [key, obj] of story.variablesState._globalVariables) {
			if (
                key.startsWith('persist_')
                || key.startsWith('trophy_')
                // If it's a soft reset, restore any objective variables
                || (soft && key.startsWith('obj_'))) {
				restore.set(key, obj);
			}	
		}

        story.ResetState();
		for (const [key, obj] of restore) {
			story.variablesState._globalVariables.set(key, obj);
		}

        setVisible(".header", true);

        continueStory();
    }

    // -----------------------------------
    // Various Helper functions
    // -----------------------------------

    // Fades in an element after a specified delay
    function showAfter(delay, el) {
        el.classList.add("hide");
        setTimeout(function() { el.classList.remove("hide") }, delay);
    }

    // Scrolls the page down, but no further than the bottom edge of what you could
    // see previously, so it doesn't go too far.
    function scrollDown(previousBottomEdge) {
        const scrollRoot = outerScrollContainer.current;
        // Line up top of screen with the bottom of where the previous content ended
        var target = previousBottomEdge;
        
        // Can't go further than the very bottom of the page
        var limit = scrollRoot.scrollHeight - scrollRoot.clientHeight;
        if( target > limit ) target = limit;

        var start = scrollRoot.scrollTop;

        var dist = target - start;
        var duration = 300 + 300*dist/100;
        var startTime = null;
        function step(time) {
            if( startTime == null ) startTime = time;
            var t = (time-startTime) / duration;
            var lerp = 3*t*t - 2*t*t*t; // ease in/out
            scrollRoot.scrollTo(0, (1.0-lerp)*start + lerp*target);
            if( t < 1 ) requestAnimationFrame(step);
        }
        requestAnimationFrame(step);
    }

    // The Y coordinate of the bottom end of all the story content, used
    // for growing the container, and deciding how far to scroll.
    function contentBottomEdgeY() {
        var bottomElement = storyContainer.current.lastElementChild;
        return bottomElement ? bottomElement.offsetTop + bottomElement.offsetHeight : 0;
    }

    // Remove all elements that match the given selector. Used for removing choices after
    // you've picked one, as well as for the CLEAR and RESTART tags.
    function removeAll(selector) {
        if (selector) {
            var allElements = storyContainer.current.querySelectorAll(selector);
            for(var i=0; i<allElements.length; i++) {
                var el = allElements[i];
                el.parentNode.removeChild(el);
            }
        } else {
            storyContainer.current.innerHTML = "";
        }
    }

    // Used for hiding and showing the header when you CLEAR or RESTART the story respectively.
    function setVisible(selector, visible)
    {
        var allElements = storyContainer.current.querySelectorAll(selector);
        for(var i=0; i<allElements.length; i++) {
            var el = allElements[i];
            if( !visible )
                el.classList.add("invisible");
            else
                el.classList.remove("invisible");
        }
    }

    // Helper for parsing out tags of the form:
    //  # PROPERTY: value
    // e.g. IMAGE: source path
    function splitPropertyTag(tag) {
        var propertySplitIdx = tag.indexOf(":");
        if( propertySplitIdx != null ) {
            var property = tag.substr(0, propertySplitIdx).trim();
            var val = tag.substr(propertySplitIdx+1).trim(); 
            return {
                property: property,
                val: val
            };
        }

        return null;
    }
}

export default InkReact;